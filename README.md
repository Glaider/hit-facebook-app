# **Facebook Application** #

### What is this repository for? ###

* Facebook Application Institute Project
* Version 1.0

### App Samples ###

* Log In

![log in.jpg](https://bitbucket.org/repo/kbnL9E/images/3561465276-log%20in.jpg)

* Account Board

![Main Panel.png](https://bitbucket.org/repo/kbnL9E/images/3934897139-Main%20Panel.png)

* Events Statistics Board

![Events Statistics.png](https://bitbucket.org/repo/kbnL9E/images/1235798974-Events%20Statistics.png)

* Print Account Info Board

![Print Panel.png](https://bitbucket.org/repo/kbnL9E/images/3022655215-Print%20Panel.png)

* Print Sample

![0001.jpg](https://bitbucket.org/repo/kbnL9E/images/1697266758-0001.jpg)
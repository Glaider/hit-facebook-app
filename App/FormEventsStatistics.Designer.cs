﻿namespace App
{
    internal partial class FormEventsStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEventsStatistics));
            this.chartEventsStatistics = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelSelectStartDate = new System.Windows.Forms.Label();
            this.dateTimePickerStatistics = new System.Windows.Forms.DateTimePicker();
            this.buttonGoStatistics = new System.Windows.Forms.Button();
            this.labelNoStatistics = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartEventsStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // chartEventsStatistics
            // 
            chartArea1.AxisX.Title = "Date";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.AxisY.Title = "Amount Of Events";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.Name = "ChartArea7Day";
            chartArea2.AxisX.Title = "Date";
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.AxisY.Title = "Amount Of People";
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea2.Name = "ChartAreaEventInvitedFriends";
            this.chartEventsStatistics.ChartAreas.Add(chartArea1);
            this.chartEventsStatistics.ChartAreas.Add(chartArea2);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend1.IsTextAutoFit = false;
            legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend1.Name = "LegendEvents";
            this.chartEventsStatistics.Legends.Add(legend1);
            this.chartEventsStatistics.Location = new System.Drawing.Point(12, 62);
            this.chartEventsStatistics.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chartEventsStatistics.Name = "chartEventsStatistics";
            this.chartEventsStatistics.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea7Day";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series1.Color = System.Drawing.Color.LightSeaGreen;
            series1.Legend = "LegendEvents";
            series1.LegendText = "Amount Of Events Per Day";
            series1.Name = "seriesEventsCount";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.ChartArea = "ChartAreaEventInvitedFriends";
            series2.Color = System.Drawing.Color.Green;
            series2.Legend = "LegendEvents";
            series2.LegendText = "Attending Users";
            series2.Name = "SeriesAttendingUsers";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series3.ChartArea = "ChartAreaEventInvitedFriends";
            series3.Color = System.Drawing.Color.Gold;
            series3.Legend = "LegendEvents";
            series3.LegendText = "Maybe Attending Users";
            series3.Name = "SeriesMaybeAttendingUsers";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.ChartArea = "ChartAreaEventInvitedFriends";
            series4.Color = System.Drawing.Color.Brown;
            series4.Legend = "LegendEvents";
            series4.LegendText = "Declined Users";
            series4.Name = "SeriesDeclinedUsers";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartEventsStatistics.Series.Add(series1);
            this.chartEventsStatistics.Series.Add(series2);
            this.chartEventsStatistics.Series.Add(series3);
            this.chartEventsStatistics.Series.Add(series4);
            this.chartEventsStatistics.Size = new System.Drawing.Size(1009, 575);
            this.chartEventsStatistics.TabIndex = 0;
            this.chartEventsStatistics.Text = "7 day Statistics";
            // 
            // labelSelectStartDate
            // 
            this.labelSelectStartDate.AutoSize = true;
            this.labelSelectStartDate.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectStartDate.Location = new System.Drawing.Point(12, 22);
            this.labelSelectStartDate.Name = "labelSelectStartDate";
            this.labelSelectStartDate.Size = new System.Drawing.Size(115, 16);
            this.labelSelectStartDate.TabIndex = 1;
            this.labelSelectStartDate.Text = "Select start date:";
            // 
            // dateTimePickerStatistics
            // 
            this.dateTimePickerStatistics.CalendarFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerStatistics.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePickerStatistics.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStatistics.Location = new System.Drawing.Point(149, 14);
            this.dateTimePickerStatistics.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerStatistics.Name = "dateTimePickerStatistics";
            this.dateTimePickerStatistics.Size = new System.Drawing.Size(200, 27);
            this.dateTimePickerStatistics.TabIndex = 1;
            // 
            // buttonGoStatistics
            // 
            this.buttonGoStatistics.BackColor = System.Drawing.Color.White;
            this.buttonGoStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonGoStatistics.Location = new System.Drawing.Point(379, 14);
            this.buttonGoStatistics.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonGoStatistics.Name = "buttonGoStatistics";
            this.buttonGoStatistics.Size = new System.Drawing.Size(103, 30);
            this.buttonGoStatistics.TabIndex = 3;
            this.buttonGoStatistics.Text = "GO";
            this.buttonGoStatistics.UseVisualStyleBackColor = false;
            this.buttonGoStatistics.Click += new System.EventHandler(this.buttonGoStatistics_Click);
            // 
            // labelNoStatistics
            // 
            this.labelNoStatistics.AutoSize = true;
            this.labelNoStatistics.Location = new System.Drawing.Point(507, 14);
            this.labelNoStatistics.Name = "labelNoStatistics";
            this.labelNoStatistics.Size = new System.Drawing.Size(86, 17);
            this.labelNoStatistics.TabIndex = 4;
            this.labelNoStatistics.Text = "No Statistics";
            this.labelNoStatistics.Visible = false;
            // 
            // FormEventsStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 52);
            this.Controls.Add(this.labelNoStatistics);
            this.Controls.Add(this.buttonGoStatistics);
            this.Controls.Add(this.dateTimePickerStatistics);
            this.Controls.Add(this.labelSelectStartDate);
            this.Controls.Add(this.chartEventsStatistics);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FormEventsStatistics";
            ((System.ComponentModel.ISupportInitialize)(this.chartEventsStatistics)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartEventsStatistics;
        private System.Windows.Forms.Label labelSelectStartDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStatistics;
        private System.Windows.Forms.Button buttonGoStatistics;
        private System.Windows.Forms.Label labelNoStatistics;
    }
}
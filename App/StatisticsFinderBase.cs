﻿using System;
using System.Collections.Generic;

namespace A16_Ex02_Stephan_321178253_Alex_323260620
{
    using System.Windows.Forms.DataVisualization.Charting;

    using FacebookWrapper.ObjectModel;

    public abstract class StatisticsFinderBase
    {
        int amountOfDeclined = 0;
        int amountOfAttending = 0;
        int amountOfMaybe = 0;
        int amountOfEvents = 0;
        int days = 1;
        
        bool isEmptyChart = true;

        protected IList<Event> EventList { get; set; }

        protected DateTime? DateChoosedForStatistics { get; set; }

        protected Chart Chart { get; set; }



        protected StatisticsFinderBase(Chart i_ChartToDisplay, IList<Event> i_EventList, DateTime i_DateChoosedForStatistics)
        {
            Chart = i_ChartToDisplay;
            EventList = i_EventList;
            DateChoosedForStatistics = i_DateChoosedForStatistics;
        }

        public void FindStatisticsDataTemplateMethod()
        {
            int startIndex = FindStartDateIndex();            
            Find7DayStats(startIndex);
        }

        public abstract int FindStartDateIndex();

        public void Find7DayStats(int i_Index)
        {
           


        }


        private void fillChartData(int i_Index)
        {
            Event fbEvent = EventList[i_Index];
            if (fbEvent.StartTime != null)
            {
                int comparison = fbEvent.StartTime.Value.Date.CompareTo(DateChoosedForStatistics);
                if (comparison == 0)
                {
                    amountOfEvents++;
                    amountOfDeclined = fbEvent.DeclinedUsers.Count;
                    amountOfAttending = fbEvent.AttendingUsers.Count;
                    amountOfMaybe = fbEvent.MaybeAttendingUsers.Count;
                    if (isEmptyChart)
                    {
                        isEmptyChart = false;
                        chartEventsStatistics.ChartAreas["ChartArea7Day"].AxisX.Minimum = dateTimePickerStatistics.Value.Date.Date.ToOADate();
                        chartEventsStatistics.ChartAreas["ChartArea7Day"].AxisX.Maximum = dateTimePickerStatistics.Value.Date.AddDays(6).Date.ToOADate();
                        chartEventsStatistics.ChartAreas["ChartAreaEventInvitedFriends"].AxisX.Minimum = dateTimePickerStatistics.Value.Date.Date.ToOADate() - 1;
                        chartEventsStatistics.ChartAreas["ChartAreaEventInvitedFriends"].AxisX.Maximum = dateTimePickerStatistics.Value.Date.AddDays(7).Date.ToOADate();
                    }
                }
                else
                {
                    addPointToChart(DateChoosedForStatistics, amountOfEvents, amountOfAttending, amountOfMaybe, amountOfDeclined);
                    amountOfDeclined = amountOfAttending = amountOfMaybe = amountOfEvents = 0;

                    i_Index++;
                    if (DateChoosedForStatistics != null)
                    {
                        DateChoosedForStatistics = DateChoosedForStatistics.Value.AddDays(1);
                    }

                    days++;
                    if (days > 7)
                    {
                        break;
                    }
                }
            }
        }

    }

}

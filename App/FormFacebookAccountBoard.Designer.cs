﻿namespace App
{
    internal partial class FormFacebookAccountBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFacebookAccountBoard));
            this.pictureBoxUserSmallPicture = new System.Windows.Forms.PictureBox();
            this.groupBoxFriends = new System.Windows.Forms.GroupBox();
            this.pictureBoxFriendsPic = new System.Windows.Forms.PictureBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBoxFriends = new System.Windows.Forms.ListBox();
            this.dataGridViewPosts = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.createdTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewEvents = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelEvents = new System.Windows.Forms.LinkLabel();
            this.labelPosts = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBoxEventPic = new System.Windows.Forms.GroupBox();
            this.buttonPostStatus = new System.Windows.Forms.Button();
            this.textBoxPostStatus = new System.Windows.Forms.TextBox();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonRefreshTable = new System.Windows.Forms.Button();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxEventNameFilter = new System.Windows.Forms.TextBox();
            this.groupBoxEventsFilter = new System.Windows.Forms.GroupBox();
            this.checkBoxEventStartDateFilter = new System.Windows.Forms.CheckBox();
            this.dateTimePickerEventStartDate = new System.Windows.Forms.DateTimePicker();
            this.labelMessage = new System.Windows.Forms.Label();
            this.textBoxEventDescriptionFilter = new System.Windows.Forms.TextBox();
            this.groupBoxPostsFilter = new System.Windows.Forms.GroupBox();
            this.checkBoxPostDateFilter = new System.Windows.Forms.CheckBox();
            this.dateTimePickerPostDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxPostTitleFilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelContentFilter = new System.Windows.Forms.Label();
            this.textBoxPostMessageFilter = new System.Windows.Forms.TextBox();
            this.buttonPrintProfileInfo = new System.Windows.Forms.Button();
            this.buttonStatistics = new System.Windows.Forms.Button();
            this.pictureBoxChartStatistics = new System.Windows.Forms.PictureBox();
            this.groupBoxEventStatistics = new System.Windows.Forms.GroupBox();
            this.checkBoxEventsCaseSensitive = new System.Windows.Forms.CheckBox();
            this.checkBoxPostsCaseSensitive = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserSmallPicture)).BeginInit();
            this.groupBoxFriends.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriendsPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBoxEventPic.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxEventsFilter.SuspendLayout();
            this.groupBoxPostsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChartStatistics)).BeginInit();
            this.groupBoxEventStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxUserSmallPicture
            // 
            this.pictureBoxUserSmallPicture.Location = new System.Drawing.Point(13, 36);
            this.pictureBoxUserSmallPicture.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxUserSmallPicture.Name = "pictureBoxUserSmallPicture";
            this.pictureBoxUserSmallPicture.Size = new System.Drawing.Size(152, 145);
            this.pictureBoxUserSmallPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxUserSmallPicture.TabIndex = 17;
            this.pictureBoxUserSmallPicture.TabStop = false;
            // 
            // groupBoxFriends
            // 
            this.groupBoxFriends.Controls.Add(this.pictureBoxFriendsPic);
            this.groupBoxFriends.Controls.Add(this.listBoxFriends);
            this.groupBoxFriends.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxFriends.Location = new System.Drawing.Point(185, 36);
            this.groupBoxFriends.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxFriends.Name = "groupBoxFriends";
            this.groupBoxFriends.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxFriends.Size = new System.Drawing.Size(493, 145);
            this.groupBoxFriends.TabIndex = 34;
            this.groupBoxFriends.TabStop = false;
            this.groupBoxFriends.Text = "Friends";
            // 
            // pictureBoxFriendsPic
            // 
            this.pictureBoxFriendsPic.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.userBindingSource, "ImageSquare", true));
            this.pictureBoxFriendsPic.Location = new System.Drawing.Point(370, 23);
            this.pictureBoxFriendsPic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxFriendsPic.Name = "pictureBoxFriendsPic";
            this.pictureBoxFriendsPic.Size = new System.Drawing.Size(117, 109);
            this.pictureBoxFriendsPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFriendsPic.TabIndex = 32;
            this.pictureBoxFriendsPic.TabStop = false;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(FacebookWrapper.ObjectModel.User);
            // 
            // listBoxFriends
            // 
            this.listBoxFriends.DataSource = this.userBindingSource;
            this.listBoxFriends.DisplayMember = "Name";
            this.listBoxFriends.FormattingEnabled = true;
            this.listBoxFriends.ItemHeight = 20;
            this.listBoxFriends.Location = new System.Drawing.Point(7, 22);
            this.listBoxFriends.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxFriends.Name = "listBoxFriends";
            this.listBoxFriends.Size = new System.Drawing.Size(356, 104);
            this.listBoxFriends.TabIndex = 19;
            this.listBoxFriends.ValueMember = "Albums";
            // 
            // dataGridViewPosts
            // 
            this.dataGridViewPosts.AllowUserToAddRows = false;
            this.dataGridViewPosts.AllowUserToDeleteRows = false;
            this.dataGridViewPosts.AllowUserToResizeColumns = false;
            this.dataGridViewPosts.AutoGenerateColumns = false;
            this.dataGridViewPosts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPosts.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewPosts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPosts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.messageDataGridViewTextBoxColumn,
            this.linkDataGridViewTextBoxColumn,
            this.createdTimeDataGridViewTextBoxColumn});
            this.dataGridViewPosts.DataSource = this.postBindingSource;
            this.dataGridViewPosts.Location = new System.Drawing.Point(13, 553);
            this.dataGridViewPosts.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewPosts.MultiSelect = false;
            this.dataGridViewPosts.Name = "dataGridViewPosts";
            this.dataGridViewPosts.ReadOnly = true;
            this.dataGridViewPosts.RowHeadersVisible = false;
            this.dataGridViewPosts.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewPosts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPosts.Size = new System.Drawing.Size(725, 146);
            this.dataGridViewPosts.TabIndex = 35;
            this.dataGridViewPosts.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPosts_CellContentDoubleClick);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Title";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Message";
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // linkDataGridViewTextBoxColumn
            // 
            this.linkDataGridViewTextBoxColumn.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn.HeaderText = "Link  (double click)";
            this.linkDataGridViewTextBoxColumn.Name = "linkDataGridViewTextBoxColumn";
            this.linkDataGridViewTextBoxColumn.ReadOnly = true;
            this.linkDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.linkDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.linkDataGridViewTextBoxColumn.ToolTipText = "Double click to open";
            // 
            // createdTimeDataGridViewTextBoxColumn
            // 
            this.createdTimeDataGridViewTextBoxColumn.DataPropertyName = "CreatedTime";
            this.createdTimeDataGridViewTextBoxColumn.HeaderText = "CreatedTime";
            this.createdTimeDataGridViewTextBoxColumn.Name = "createdTimeDataGridViewTextBoxColumn";
            this.createdTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postBindingSource
            // 
            this.postBindingSource.DataSource = typeof(FacebookWrapper.ObjectModel.Post);
            // 
            // dataGridViewEvents
            // 
            this.dataGridViewEvents.AllowUserToAddRows = false;
            this.dataGridViewEvents.AllowUserToDeleteRows = false;
            this.dataGridViewEvents.AllowUserToResizeColumns = false;
            this.dataGridViewEvents.AutoGenerateColumns = false;
            this.dataGridViewEvents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEvents.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn1,
            this.descriptionDataGridViewTextBoxColumn,
            this.startTimeDataGridViewTextBoxColumn,
            this.endTimeDataGridViewTextBoxColumn});
            this.dataGridViewEvents.DataSource = this.eventBindingSource;
            this.dataGridViewEvents.Location = new System.Drawing.Point(13, 376);
            this.dataGridViewEvents.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewEvents.MultiSelect = false;
            this.dataGridViewEvents.Name = "dataGridViewEvents";
            this.dataGridViewEvents.ReadOnly = true;
            this.dataGridViewEvents.RowHeadersVisible = false;
            this.dataGridViewEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEvents.Size = new System.Drawing.Size(725, 149);
            this.dataGridViewEvents.TabIndex = 36;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // startTimeDataGridViewTextBoxColumn
            // 
            this.startTimeDataGridViewTextBoxColumn.DataPropertyName = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.HeaderText = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.Name = "startTimeDataGridViewTextBoxColumn";
            this.startTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // endTimeDataGridViewTextBoxColumn
            // 
            this.endTimeDataGridViewTextBoxColumn.DataPropertyName = "EndTime";
            this.endTimeDataGridViewTextBoxColumn.HeaderText = "EndTime";
            this.endTimeDataGridViewTextBoxColumn.Name = "endTimeDataGridViewTextBoxColumn";
            this.endTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // eventBindingSource
            // 
            this.eventBindingSource.DataSource = typeof(FacebookWrapper.ObjectModel.Event);
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.Location = new System.Drawing.Point(13, 12);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(64, 25);
            this.labelUserName.TabIndex = 38;
            this.labelUserName.Text = "label1";
            // 
            // labelEvents
            // 
            this.labelEvents.AutoSize = true;
            this.labelEvents.LinkArea = new System.Windows.Forms.LinkArea(0, 13);
            this.labelEvents.Location = new System.Drawing.Point(5, 21);
            this.labelEvents.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEvents.Name = "labelEvents";
            this.labelEvents.Size = new System.Drawing.Size(242, 38);
            this.labelEvents.TabIndex = 49;
            this.labelEvents.TabStop = true;
            this.labelEvents.Text = "Fetch Events \r\n(Click on an event to view it\'s picture)";
            this.labelEvents.UseCompatibleTextRendering = true;
            this.labelEvents.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labelEvents_LinkClicked);
            // 
            // labelPosts
            // 
            this.labelPosts.AutoSize = true;
            this.labelPosts.LinkArea = new System.Windows.Forms.LinkArea(0, 13);
            this.labelPosts.Location = new System.Drawing.Point(13, 529);
            this.labelPosts.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPosts.Name = "labelPosts";
            this.labelPosts.Size = new System.Drawing.Size(86, 20);
            this.labelPosts.TabIndex = 50;
            this.labelPosts.TabStop = true;
            this.labelPosts.Text = "Fetch Posts \r\n";
            this.labelPosts.UseCompatibleTextRendering = true;
            this.labelPosts.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labelPosts_LinkClicked);
            // 
            // pictureBox2
            // 
            this.pictureBox2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.eventBindingSource, "ImageSqaure", true));
            this.pictureBox2.Location = new System.Drawing.Point(280, 10);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(83, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // groupBoxEventPic
            // 
            this.groupBoxEventPic.Controls.Add(this.labelEvents);
            this.groupBoxEventPic.Controls.Add(this.pictureBox2);
            this.groupBoxEventPic.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxEventPic.Location = new System.Drawing.Point(13, 298);
            this.groupBoxEventPic.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxEventPic.Name = "groupBoxEventPic";
            this.groupBoxEventPic.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxEventPic.Size = new System.Drawing.Size(369, 74);
            this.groupBoxEventPic.TabIndex = 38;
            this.groupBoxEventPic.TabStop = false;
            // 
            // buttonPostStatus
            // 
            this.buttonPostStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPostStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(84)))), ((int)(((byte)(146)))));
            this.buttonPostStatus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPostStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPostStatus.Location = new System.Drawing.Point(517, 26);
            this.buttonPostStatus.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPostStatus.Name = "buttonPostStatus";
            this.buttonPostStatus.Size = new System.Drawing.Size(141, 57);
            this.buttonPostStatus.TabIndex = 52;
            this.buttonPostStatus.Text = "P O S T";
            this.buttonPostStatus.UseVisualStyleBackColor = false;
            this.buttonPostStatus.Click += new System.EventHandler(this.buttonPostStatus_Click);
            // 
            // textBoxPostStatus
            // 
            this.textBoxPostStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPostStatus.Location = new System.Drawing.Point(7, 26);
            this.textBoxPostStatus.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPostStatus.Multiline = true;
            this.textBoxPostStatus.Name = "textBoxPostStatus";
            this.textBoxPostStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxPostStatus.Size = new System.Drawing.Size(508, 57);
            this.textBoxPostStatus.TabIndex = 51;
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.BackColor = System.Drawing.Color.White;
            this.buttonLogOut.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogOut.ForeColor = System.Drawing.Color.Red;
            this.buttonLogOut.Location = new System.Drawing.Point(861, 9);
            this.buttonLogOut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(129, 28);
            this.buttonLogOut.TabIndex = 54;
            this.buttonLogOut.Text = "LogOut";
            this.buttonLogOut.UseVisualStyleBackColor = false;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonPostStatus);
            this.groupBox1.Controls.Add(this.textBoxPostStatus);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(13, 197);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(665, 99);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Post status:";
            // 
            // buttonRefreshTable
            // 
            this.buttonRefreshTable.BackColor = System.Drawing.Color.White;
            this.buttonRefreshTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRefreshTable.Location = new System.Drawing.Point(745, 9);
            this.buttonRefreshTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonRefreshTable.Name = "buttonRefreshTable";
            this.buttonRefreshTable.Size = new System.Drawing.Size(110, 28);
            this.buttonRefreshTable.TabIndex = 55;
            this.buttonRefreshTable.Text = "Refresh";
            this.buttonRefreshTable.UseVisualStyleBackColor = false;
            this.buttonRefreshTable.Click += new System.EventHandler(this.buttonRefreshTable_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(8, 34);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(48, 17);
            this.labelName.TabIndex = 57;
            this.labelName.Text = "Name:";
            // 
            // textBoxEventNameFilter
            // 
            this.textBoxEventNameFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEventNameFilter.Location = new System.Drawing.Point(80, 27);
            this.textBoxEventNameFilter.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEventNameFilter.Name = "textBoxEventNameFilter";
            this.textBoxEventNameFilter.Size = new System.Drawing.Size(162, 24);
            this.textBoxEventNameFilter.TabIndex = 56;
            this.textBoxEventNameFilter.TextChanged += new System.EventHandler(this.eventsFilterField_ValueChange);
            // 
            // groupBoxEventsFilter
            // 
            this.groupBoxEventsFilter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBoxEventsFilter.Controls.Add(this.checkBoxEventsCaseSensitive);
            this.groupBoxEventsFilter.Controls.Add(this.checkBoxEventStartDateFilter);
            this.groupBoxEventsFilter.Controls.Add(this.dateTimePickerEventStartDate);
            this.groupBoxEventsFilter.Controls.Add(this.labelMessage);
            this.groupBoxEventsFilter.Controls.Add(this.textBoxEventDescriptionFilter);
            this.groupBoxEventsFilter.Controls.Add(this.labelName);
            this.groupBoxEventsFilter.Controls.Add(this.textBoxEventNameFilter);
            this.groupBoxEventsFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxEventsFilter.Location = new System.Drawing.Point(741, 376);
            this.groupBoxEventsFilter.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxEventsFilter.Name = "groupBoxEventsFilter";
            this.groupBoxEventsFilter.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxEventsFilter.Size = new System.Drawing.Size(249, 149);
            this.groupBoxEventsFilter.TabIndex = 58;
            this.groupBoxEventsFilter.TabStop = false;
            this.groupBoxEventsFilter.Text = "Events Filter";
            // 
            // checkBoxEventStartDateFilter
            // 
            this.checkBoxEventStartDateFilter.AutoSize = true;
            this.checkBoxEventStartDateFilter.Location = new System.Drawing.Point(3, 121);
            this.checkBoxEventStartDateFilter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBoxEventStartDateFilter.Name = "checkBoxEventStartDateFilter";
            this.checkBoxEventStartDateFilter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxEventStartDateFilter.Size = new System.Drawing.Size(93, 21);
            this.checkBoxEventStartDateFilter.TabIndex = 60;
            this.checkBoxEventStartDateFilter.Text = "Start Date";
            this.checkBoxEventStartDateFilter.UseVisualStyleBackColor = true;
            this.checkBoxEventStartDateFilter.CheckedChanged += new System.EventHandler(this.checkBoxFilter_CheckedChanged);
            // 
            // dateTimePickerEventStartDate
            // 
            this.dateTimePickerEventStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerEventStartDate.Enabled = false;
            this.dateTimePickerEventStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEventStartDate.Location = new System.Drawing.Point(105, 117);
            this.dateTimePickerEventStartDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerEventStartDate.Name = "dateTimePickerEventStartDate";
            this.dateTimePickerEventStartDate.Size = new System.Drawing.Size(137, 24);
            this.dateTimePickerEventStartDate.TabIndex = 59;
            this.dateTimePickerEventStartDate.ValueChanged += new System.EventHandler(this.eventsFilterField_ValueChange);
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMessage.Location = new System.Drawing.Point(8, 78);
            this.labelMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(81, 17);
            this.labelMessage.TabIndex = 60;
            this.labelMessage.Text = "Description:";
            // 
            // textBoxEventDescriptionFilter
            // 
            this.textBoxEventDescriptionFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxEventDescriptionFilter.Location = new System.Drawing.Point(97, 74);
            this.textBoxEventDescriptionFilter.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEventDescriptionFilter.Name = "textBoxEventDescriptionFilter";
            this.textBoxEventDescriptionFilter.Size = new System.Drawing.Size(145, 24);
            this.textBoxEventDescriptionFilter.TabIndex = 59;
            this.textBoxEventDescriptionFilter.TextChanged += new System.EventHandler(this.eventsFilterField_ValueChange);
            // 
            // groupBoxPostsFilter
            // 
            this.groupBoxPostsFilter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBoxPostsFilter.Controls.Add(this.checkBoxPostsCaseSensitive);
            this.groupBoxPostsFilter.Controls.Add(this.checkBoxPostDateFilter);
            this.groupBoxPostsFilter.Controls.Add(this.dateTimePickerPostDate);
            this.groupBoxPostsFilter.Controls.Add(this.textBoxPostTitleFilter);
            this.groupBoxPostsFilter.Controls.Add(this.label1);
            this.groupBoxPostsFilter.Controls.Add(this.labelContentFilter);
            this.groupBoxPostsFilter.Controls.Add(this.textBoxPostMessageFilter);
            this.groupBoxPostsFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxPostsFilter.Location = new System.Drawing.Point(745, 555);
            this.groupBoxPostsFilter.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxPostsFilter.Name = "groupBoxPostsFilter";
            this.groupBoxPostsFilter.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxPostsFilter.Size = new System.Drawing.Size(245, 149);
            this.groupBoxPostsFilter.TabIndex = 59;
            this.groupBoxPostsFilter.TabStop = false;
            this.groupBoxPostsFilter.Text = "Postss Filter";
            // 
            // checkBoxPostDateFilter
            // 
            this.checkBoxPostDateFilter.AutoSize = true;
            this.checkBoxPostDateFilter.Location = new System.Drawing.Point(7, 123);
            this.checkBoxPostDateFilter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBoxPostDateFilter.Name = "checkBoxPostDateFilter";
            this.checkBoxPostDateFilter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxPostDateFilter.Size = new System.Drawing.Size(90, 21);
            this.checkBoxPostDateFilter.TabIndex = 9;
            this.checkBoxPostDateFilter.Text = "Post Date";
            this.checkBoxPostDateFilter.UseVisualStyleBackColor = true;
            this.checkBoxPostDateFilter.CheckedChanged += new System.EventHandler(this.checkBoxFilter_CheckedChanged);
            // 
            // dateTimePickerPostDate
            // 
            this.dateTimePickerPostDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerPostDate.Enabled = false;
            this.dateTimePickerPostDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPostDate.Location = new System.Drawing.Point(107, 119);
            this.dateTimePickerPostDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerPostDate.Name = "dateTimePickerPostDate";
            this.dateTimePickerPostDate.Size = new System.Drawing.Size(131, 24);
            this.dateTimePickerPostDate.TabIndex = 8;
            this.dateTimePickerPostDate.ValueChanged += new System.EventHandler(this.postsFilterField_ValueChange);
            // 
            // textBoxPostTitleFilter
            // 
            this.textBoxPostTitleFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPostTitleFilter.Location = new System.Drawing.Point(80, 28);
            this.textBoxPostTitleFilter.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPostTitleFilter.Name = "textBoxPostTitleFilter";
            this.textBoxPostTitleFilter.Size = new System.Drawing.Size(158, 24);
            this.textBoxPostTitleFilter.TabIndex = 7;
            this.textBoxPostTitleFilter.TextChanged += new System.EventHandler(this.postsFilterField_ValueChange);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Title:";
            // 
            // labelContentFilter
            // 
            this.labelContentFilter.AutoSize = true;
            this.labelContentFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContentFilter.Location = new System.Drawing.Point(8, 79);
            this.labelContentFilter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelContentFilter.Name = "labelContentFilter";
            this.labelContentFilter.Size = new System.Drawing.Size(64, 17);
            this.labelContentFilter.TabIndex = 5;
            this.labelContentFilter.Text = "Message:";
            // 
            // textBoxPostMessageFilter
            // 
            this.textBoxPostMessageFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPostMessageFilter.Location = new System.Drawing.Point(80, 79);
            this.textBoxPostMessageFilter.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPostMessageFilter.Name = "textBoxPostMessageFilter";
            this.textBoxPostMessageFilter.Size = new System.Drawing.Size(158, 24);
            this.textBoxPostMessageFilter.TabIndex = 4;
            this.textBoxPostMessageFilter.TextChanged += new System.EventHandler(this.postsFilterField_ValueChange);
            // 
            // buttonPrintProfileInfo
            // 
            this.buttonPrintProfileInfo.BackColor = System.Drawing.Color.White;
            this.buttonPrintProfileInfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPrintProfileInfo.Location = new System.Drawing.Point(745, 58);
            this.buttonPrintProfileInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPrintProfileInfo.Name = "buttonPrintProfileInfo";
            this.buttonPrintProfileInfo.Size = new System.Drawing.Size(244, 41);
            this.buttonPrintProfileInfo.TabIndex = 60;
            this.buttonPrintProfileInfo.Text = "Print Profile Info";
            this.buttonPrintProfileInfo.UseVisualStyleBackColor = false;
            this.buttonPrintProfileInfo.Click += new System.EventHandler(this.buttonPrintProfileInfo_Click);
            // 
            // buttonStatistics
            // 
            this.buttonStatistics.BackColor = System.Drawing.Color.White;
            this.buttonStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonStatistics.Location = new System.Drawing.Point(20, 36);
            this.buttonStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.Size = new System.Drawing.Size(214, 35);
            this.buttonStatistics.TabIndex = 61;
            this.buttonStatistics.Text = "Generate Graph";
            this.buttonStatistics.UseVisualStyleBackColor = false;
            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
            // 
            // pictureBoxChartStatistics
            // 
            this.pictureBoxChartStatistics.Image = global::App.Properties.Resources.NoDataAvailableChart;
            this.pictureBoxChartStatistics.InitialImage = null;
            this.pictureBoxChartStatistics.Location = new System.Drawing.Point(20, 83);
            this.pictureBoxChartStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxChartStatistics.Name = "pictureBoxChartStatistics";
            this.pictureBoxChartStatistics.Size = new System.Drawing.Size(214, 141);
            this.pictureBoxChartStatistics.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxChartStatistics.TabIndex = 62;
            this.pictureBoxChartStatistics.TabStop = false;
            // 
            // groupBoxEventStatistics
            // 
            this.groupBoxEventStatistics.Controls.Add(this.buttonStatistics);
            this.groupBoxEventStatistics.Controls.Add(this.pictureBoxChartStatistics);
            this.groupBoxEventStatistics.Location = new System.Drawing.Point(745, 114);
            this.groupBoxEventStatistics.Name = "groupBoxEventStatistics";
            this.groupBoxEventStatistics.Size = new System.Drawing.Size(241, 243);
            this.groupBoxEventStatistics.TabIndex = 63;
            this.groupBoxEventStatistics.TabStop = false;
            this.groupBoxEventStatistics.Text = "Events Statitics";
            // 
            // checkBoxEventsCaseSensitive
            // 
            this.checkBoxEventsCaseSensitive.AutoSize = true;
            this.checkBoxEventsCaseSensitive.Location = new System.Drawing.Point(129, 0);
            this.checkBoxEventsCaseSensitive.Name = "checkBoxEventsCaseSensitive";
            this.checkBoxEventsCaseSensitive.Size = new System.Drawing.Size(116, 21);
            this.checkBoxEventsCaseSensitive.TabIndex = 61;
            this.checkBoxEventsCaseSensitive.Text = "Case Sensitive";
            this.checkBoxEventsCaseSensitive.UseVisualStyleBackColor = true;
            this.checkBoxEventsCaseSensitive.CheckedChanged += new System.EventHandler(this.eventsFilterField_ValueChange);
            // 
            // checkBoxPostsCaseSensitive
            // 
            this.checkBoxPostsCaseSensitive.AutoSize = true;
            this.checkBoxPostsCaseSensitive.Location = new System.Drawing.Point(125, 0);
            this.checkBoxPostsCaseSensitive.Name = "checkBoxPostsCaseSensitive";
            this.checkBoxPostsCaseSensitive.Size = new System.Drawing.Size(116, 21);
            this.checkBoxPostsCaseSensitive.TabIndex = 62;
            this.checkBoxPostsCaseSensitive.Text = "Case Sensitive";
            this.checkBoxPostsCaseSensitive.UseVisualStyleBackColor = true;
            this.checkBoxPostsCaseSensitive.CheckedChanged += new System.EventHandler(this.postsFilterField_ValueChange);
            // 
            // FormFacebookAccountBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 711);
            this.Controls.Add(this.groupBoxEventStatistics);
            this.Controls.Add(this.buttonPrintProfileInfo);
            this.Controls.Add(this.groupBoxPostsFilter);
            this.Controls.Add(this.groupBoxEventsFilter);
            this.Controls.Add(this.buttonRefreshTable);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonLogOut);
            this.Controls.Add(this.groupBoxEventPic);
            this.Controls.Add(this.labelPosts);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.dataGridViewEvents);
            this.Controls.Add(this.dataGridViewPosts);
            this.Controls.Add(this.groupBoxFriends);
            this.Controls.Add(this.pictureBoxUserSmallPicture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "FormFacebookAccountBoard";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUserSmallPicture)).EndInit();
            this.groupBoxFriends.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriendsPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBoxEventPic.ResumeLayout(false);
            this.groupBoxEventPic.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxEventsFilter.ResumeLayout(false);
            this.groupBoxEventsFilter.PerformLayout();
            this.groupBoxPostsFilter.ResumeLayout(false);
            this.groupBoxPostsFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChartStatistics)).EndInit();
            this.groupBoxEventStatistics.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxUserSmallPicture;
        private System.Windows.Forms.GroupBox groupBoxFriends;
        private System.Windows.Forms.PictureBox pictureBoxFriendsPic;
        private System.Windows.Forms.ListBox listBoxFriends;
        private System.Windows.Forms.DataGridView dataGridViewPosts;
        private System.Windows.Forms.BindingSource postBindingSource;
        private System.Windows.Forms.DataGridView dataGridViewEvents;
        private System.Windows.Forms.BindingSource eventBindingSource;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.LinkLabel labelEvents;
        private System.Windows.Forms.LinkLabel labelPosts;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBoxEventPic;
        private System.Windows.Forms.Button buttonPostStatus;
        private System.Windows.Forms.TextBox textBoxPostStatus;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonRefreshTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxEventNameFilter;
        private System.Windows.Forms.GroupBox groupBoxEventsFilter;
        private System.Windows.Forms.CheckBox checkBoxEventStartDateFilter;
        private System.Windows.Forms.DateTimePicker dateTimePickerEventStartDate;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.TextBox textBoxEventDescriptionFilter;
        private System.Windows.Forms.GroupBox groupBoxPostsFilter;
        private System.Windows.Forms.CheckBox checkBoxPostDateFilter;
        private System.Windows.Forms.DateTimePicker dateTimePickerPostDate;
        private System.Windows.Forms.TextBox textBoxPostTitleFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelContentFilter;
        private System.Windows.Forms.TextBox textBoxPostMessageFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewLinkColumn linkDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonPrintProfileInfo;
        private System.Windows.Forms.Button buttonStatistics;
        private System.Windows.Forms.PictureBox pictureBoxChartStatistics;
        private System.Windows.Forms.GroupBox groupBoxEventStatistics;
        private System.Windows.Forms.CheckBox checkBoxEventsCaseSensitive;
        private System.Windows.Forms.CheckBox checkBoxPostsCaseSensitive;
    }
}
﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections.Generic;
using FacebookWrapper.ObjectModel;

namespace App
{
    internal partial class FormEventsStatistics : Form
    {
        private DateTime? DateChoosedForStatistics { get; set; }
        
        private bool IsShowingChart { get; set; }

        private IList<Event> EventList { get; set; }

        private readonly Action<Chart, bool> convertChartToImage;

        public FormEventsStatistics(IList<Event> i_EventList, Action<Chart, bool> i_ChartAction)
        {
            InitializeComponent();
            convertChartToImage = i_ChartAction;
            EventList = i_EventList;                      
        }

        private bool count7Days(int i_Index)
        {
            int amountOfDeclined = 0;
            int amountOfAttending = 0;
            int amountOfMaybe = 0;
            int amountOfEvents = 0;
            int days = 1;
            DateTime? dateToAdd = DateChoosedForStatistics;
            bool isEmptyChart = true;

            for (int i = i_Index; i >= 0; i--)
            {
                Event fbEvent = EventList[i];
                if (fbEvent.StartTime != null)
                {
                    int comparison = fbEvent.StartTime.Value.Date.CompareTo(dateToAdd);
                    if (comparison == 0)
                    {
                        amountOfEvents++;
                        amountOfDeclined = fbEvent.DeclinedUsers.Count;
                        amountOfAttending = fbEvent.AttendingUsers.Count;
                        amountOfMaybe = fbEvent.MaybeAttendingUsers.Count;
                        if (isEmptyChart)
                        {
                            isEmptyChart = false;
                            chartEventsStatistics.ChartAreas["ChartArea7Day"].AxisX.Minimum = dateTimePickerStatistics.Value.Date.Date.ToOADate();
                            chartEventsStatistics.ChartAreas["ChartArea7Day"].AxisX.Maximum = dateTimePickerStatistics.Value.Date.AddDays(6).Date.ToOADate();
                            chartEventsStatistics.ChartAreas["ChartAreaEventInvitedFriends"].AxisX.Minimum = dateTimePickerStatistics.Value.Date.Date.ToOADate() - 1;
                            chartEventsStatistics.ChartAreas["ChartAreaEventInvitedFriends"].AxisX.Maximum = dateTimePickerStatistics.Value.Date.AddDays(7).Date.ToOADate();
                        }
                    }
                    else
                    {
                        addPointToChart(dateToAdd, amountOfEvents, amountOfAttending, amountOfMaybe, amountOfDeclined);
                        amountOfDeclined = amountOfAttending = amountOfMaybe = amountOfEvents = 0;

                        i++;
                        if (dateToAdd != null)
                        {
                            dateToAdd = dateToAdd.Value.AddDays(1);
                        }

                        days++;
                        if (days > 7)
                        {
                            break;
                        }
                    }
                }
            }

            while (days < 8)
            {
                if (dateToAdd != null)
                {
                    addPointToChart(dateToAdd, amountOfEvents, amountOfAttending, amountOfMaybe, amountOfDeclined);
                }

                amountOfDeclined = amountOfAttending = amountOfMaybe = amountOfEvents = 0;
                days++;
            }

            return isEmptyChart;
        }

        private void addPointToChart(DateTime? i_DateToAdd, int i_AmountOfEvents, int i_AmountOfAttending, int i_AmountOfMaybe, int i_AmountOfDeclined)
        {
            if (i_DateToAdd != null)
            {
                chartEventsStatistics.Series["seriesEventsCount"].Points.AddXY(i_DateToAdd, i_AmountOfEvents);
                chartEventsStatistics.Series["SeriesAttendingUsers"].Points.AddXY(i_DateToAdd, i_AmountOfAttending);
                chartEventsStatistics.Series["SeriesMaybeAttendingUsers"].Points.AddXY(i_DateToAdd, i_AmountOfMaybe);
                chartEventsStatistics.Series["SeriesDeclinedUsers"].Points.AddXY(i_DateToAdd, i_AmountOfDeclined);
            }
        }

        private void initChart()
        {
            bool isEmptStatistics = true;
            for (int i = EventList.Count - 1; i >= 0; i--)
            {
                var startTime = EventList[i].StartTime;
                if (startTime != null)
                {
                    int comparison = startTime.Value.Date.CompareTo(DateChoosedForStatistics);

                    if (comparison == -1)
                    {
                        continue;
                    }

                    isEmptStatistics = count7Days(i);
                    break;
                }
            }

            showCorrectWindowSize(isEmptStatistics);
        }

        private void showCorrectWindowSize(bool i_IsEmptyStatistics)
        {
            if (!i_IsEmptyStatistics)
            {               
                Height = 567;
                Width = 790;
                labelNoStatistics.Visible = false;
                IsShowingChart = true;
            }
            else
            {             
                Height = 90;
                Width = 675;
                if (DateChoosedForStatistics != null)
                {
                    labelNoStatistics.Text = string.Format(
                        @" No events for:
                    {0} - {1}",
                        DateChoosedForStatistics.Value.Date.ToString("dd/MM/yyyy"),
                        DateChoosedForStatistics.Value.Date.AddDays(6).ToString("dd/MM/yyyy"));
                }
 
                labelNoStatistics.Visible = true;
                IsShowingChart = false;
            }         
        }

        private void buttonGoStatistics_Click(object sender, EventArgs e)
        {
            DateChoosedForStatistics = dateTimePickerStatistics.Value.Date;
            initChart();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.UserClosing)
            {
                convertChartToImage.Invoke(chartEventsStatistics, IsShowingChart);
            }
        }
    }
}

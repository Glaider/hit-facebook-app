﻿namespace App
{
    public interface IPrintComposerAdaptable
    {
         PrintField GetNamePrintField { get; }

         PrintField GetBirthDayPrintField { get; }

         PrintField GetEmailPrintField { get; }

         PrintField GetLinkPrintField { get; }

         PrintField GetLocalePrintField { get; }

         PrintField GetBioPrintField { get; }

         PrintField GetProfilePicturePrintField { get; }
    }
}

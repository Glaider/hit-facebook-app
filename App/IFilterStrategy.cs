﻿namespace App
{
    public interface IFilterStrategy
    {
        bool CheckIfApplicable(string i_TheCompared, string i_CompareTo);
    }
}

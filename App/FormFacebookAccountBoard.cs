﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace App
{
    internal partial class FormFacebookAccountBoard : Form
    {
        public IDataProducer DataProducer { get; set; }

        public Image ChartImage { get; private set; }

        public FormFacebookAccountBoard(IDataProducer i_DataProducer)
        {
            DataProducer = i_DataProducer;
            ChartImage = null;
            InitializeComponent();
            initializeData();
        }

        private void initializeData()
        {
            Text = string.Format("Logged In as {0}", DataProducer.GetLoginData.LoggedInUser.Name);
            if (!string.IsNullOrEmpty(DataProducer.GetLoginData.LoggedInUser.PictureNormalURL))
            {
                pictureBoxUserSmallPicture.LoadAsync(DataProducer.GetLoginData.LoggedInUser.PictureNormalURL);
            }

            labelUserName.Text = DataProducer.GetLoginData.LoggedInUser.Name;
            userBindingSource.DataSource = DataProducer.GetLoginData.LoggedInUser.Friends;
        }

        private void labelEvents_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (eventBindingSource.Count.Equals(0))
            {
                new Thread(fetchEvents).Start();
            }
        }

        private void labelPosts_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (postBindingSource.Count.Equals(0))
            {
                new Thread(fetchPosts).Start();
            }
        }

        private void fetchEvents()
        {
            var events = DataProducer.GetEvents;

            if (!dataGridViewEvents.InvokeRequired)
            {
                eventBindingSource.DataSource = events;
            }
            else
            {
                dataGridViewEvents.Invoke(new Action(() => eventBindingSource.DataSource = events));
            }
        }
     
        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            DataProducer.Finish();
            DialogResult = DialogResult.Retry;
        }

        private void buttonPostStatus_Click(object sender, EventArgs e)
        {
            if (textBoxPostStatus.Text.Length != 0)
            {
                try
                {
                    DataProducer.Post(textBoxPostStatus.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, @"Error ." + ex.Message, @"Oooops...", MessageBoxButtons.OK);
                }

                textBoxPostStatus.Clear();
            }
        }

        private void buttonRefreshTable_Click(object sender, EventArgs e)
        {
            resetEditableComponents();
            DataProducer.ReFetch();
            initializeData();
            new Thread(fetchEvents).Start();
            new Thread(fetchPosts).Start();
        }

        private void resetEditableComponents()
        {
            textBoxPostStatus.Clear();
            textBoxEventNameFilter.Clear();
            textBoxEventDescriptionFilter.Clear();
            textBoxPostTitleFilter.Clear();
            textBoxPostMessageFilter.Clear();
            checkBoxEventStartDateFilter.Checked = false;
            checkBoxPostDateFilter.Checked = false;
            userBindingSource.Position = 0;
            postBindingSource.Position = 0;
            eventBindingSource.Position = 0;
        }

        private void fetchPosts()
        {
            var posts = DataProducer.GetPosts;

            if (!dataGridViewPosts.InvokeRequired)
            {
                postBindingSource.DataSource = posts;
            }
            else
            {
                dataGridViewPosts.Invoke(new Action(() => postBindingSource.DataSource = posts));
            }
        }

        private void eventsFilterField_ValueChange(object sender, EventArgs e)
        {
            DateTime? date = checkBoxEventStartDateFilter.Checked ? dateTimePickerEventStartDate.Value.Date : (DateTime?)null;
            DataProducer.FilterEvents(new FBFilterArgs(textBoxEventNameFilter.Text, textBoxEventDescriptionFilter.Text, date, checkBoxEventsCaseSensitive.Checked));
            new Thread(fetchEvents).Start();
        }

        private void postsFilterField_ValueChange(object sender, EventArgs e)
        {
            DateTime? date = checkBoxPostDateFilter.Checked ? dateTimePickerPostDate.Value.Date : (DateTime?)null;
            DataProducer.FilterPosts(new FBFilterArgs(textBoxPostTitleFilter.Text, textBoxPostMessageFilter.Text, date, checkBoxPostsCaseSensitive.Checked));
            new Thread(fetchPosts).Start();
        }

        private void checkBoxFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == checkBoxEventStartDateFilter)
            {
                dateTimePickerEventStartDate.Enabled = !dateTimePickerEventStartDate.Enabled;
                eventsFilterField_ValueChange(null, null);
            }
            else if (sender == checkBoxPostDateFilter)
            {
                dateTimePickerPostDate.Enabled = !dateTimePickerPostDate.Enabled;
                postsFilterField_ValueChange(null, null);
            }
        }

        private void dataGridViewPosts_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((DataGridView)sender).Columns[e.ColumnIndex] is DataGridViewLinkColumn)
            {
                string link = ((DataGridView)sender).CurrentCell.Value as string ?? string.Empty;
                if (!string.IsNullOrEmpty(link))
                {
                    System.Diagnostics.Process.Start(link);
                }
            }
        }

        private void buttonPrintProfileInfo_Click(object sender, EventArgs e)
        {
            new FormDocumentComposer(new PrintComposerAdapter(DataProducer), ChartImage).ShowDialog();
        }

        private void buttonStatistics_Click(object sender, EventArgs e)
        {
            new FormEventsStatistics(DataProducer.GetLoginData.LoggedInUser.Events, generateChartImage).ShowDialog();
        }

        private void generateChartImage(Chart i_Chart, bool i_isNotEmptyChart)
        {
            if (i_isNotEmptyChart)
            {
                Stream stream = new MemoryStream();
                i_Chart.SaveImage(stream, ChartImageFormat.Jpeg);
                ChartImage = Image.FromStream(stream);
                pictureBoxChartStatistics.Image = ChartImage;
            }
            else
            {
                ChartImage = null;
                pictureBoxChartStatistics.Image = Properties.Resources.NoDataAvailableChart;
            }
        }
    }
}

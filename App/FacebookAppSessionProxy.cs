﻿using System.Collections.Generic;
using System.Threading;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;

namespace App
{
    public class FacebookAppSessionProxy : IDataProducer
    {
        private FacebookAppSession AppSession { get; set; }

        private IList<Post> m_CachedPostsList;

        private IList<Event> m_CachedEventsList;

        private IFilterStrategy m_FilterStrategy;

        public FacebookAppSessionProxy(FacebookAppSession i_AppSesion)
        {
            AppSession = i_AppSesion;
            m_CachedPostsList = AppSession.Result.LoggedInUser.Posts;
            m_CachedEventsList = AppSession.Result.LoggedInUser.Events;
        }

        public LoginResult GetLoginData
        {
            get
            {
                return AppSession.GetLoginData;
            }
        }

        public void Finish()
        {
            AppSession.Finish();
        }

        public IList<Post> GetPosts
        {
            get
            {
                return m_CachedPostsList;
            }
        }

        public IList<Event> GetEvents
        {
            get
            {
                return m_CachedEventsList;
            }
        }

        public void InitializeData(string i_Token = null)
        {
            AppSession.InitializeData(i_Token);
        }

        public void ReFetch()
        {
            new Thread(() =>
            {
                AppSession.ReFetch();
                m_CachedPostsList = AppSession.Result.LoggedInUser.Posts;
                m_CachedEventsList = AppSession.Result.LoggedInUser.Events;
            }).Start();
        }

        public void FilterEvents(FBFilterArgs i_FilterArgs)
        {
            correctStrategy(i_FilterArgs.IsCaseSensitive);
            m_CachedEventsList = new List<Event>();
            foreach (Event fbEvent in AppSession.GetEvents)
            {
                var isDateSelectedAndMatch = fbEvent.StartTime != null && (i_FilterArgs.StartDateOrPostDate == null || fbEvent.StartTime.Value.Date.Equals(i_FilterArgs.StartDateOrPostDate));

                if (m_FilterStrategy.CheckIfApplicable(fbEvent.Name, i_FilterArgs.NameOrTitle))
                {
                    if (m_FilterStrategy.CheckIfApplicable(fbEvent.Description, i_FilterArgs.MessageOrDescription) && isDateSelectedAndMatch)
                    {
                        m_CachedEventsList.Add(fbEvent);
                    }
                }
            }
        }

        public void Post(string i_TextPost)
        {
            new Thread(() =>
            {
                if (i_TextPost != null)
                {
                    AppSession.Post(i_TextPost);
                }
            }).Start();
        }

        private void correctStrategy(bool i_IsCaseSens)
        {
            if (i_IsCaseSens && !(m_FilterStrategy is CaseSensitiveFilterStrategy))
            {
                m_FilterStrategy = new CaseSensitiveFilterStrategy();
            }
            else if (!i_IsCaseSens && !(m_FilterStrategy is NoCaseSensitiveFilterStrategy))
            {
                m_FilterStrategy = new NoCaseSensitiveFilterStrategy();
            }
        }

        public void FilterPosts(FBFilterArgs i_FilterArgs)
        {
            correctStrategy(i_FilterArgs.IsCaseSensitive);
            m_CachedPostsList = new List<Post>();
            foreach (Post post in AppSession.GetPosts)
            {
                var isDateSelectedAndMatch = post.CreatedTime != null && (i_FilterArgs.StartDateOrPostDate == null || post.CreatedTime.Value.Date.Equals(i_FilterArgs.StartDateOrPostDate));
                if (m_FilterStrategy.CheckIfApplicable(post.Name, i_FilterArgs.NameOrTitle))
                {
                    if (m_FilterStrategy.CheckIfApplicable(post.Message, i_FilterArgs.MessageOrDescription) && isDateSelectedAndMatch)
                    {
                        m_CachedPostsList.Add(post);
                    }
                }
            }
        }
    }
}

﻿using System;

namespace App
{
    public class FBFilterArgs : EventArgs
    {
        public string NameOrTitle { get; set; }

        public string MessageOrDescription { get; set; }

        public DateTime? StartDateOrPostDate { get; set; }

        public bool IsCaseSensitive { get; set; }

        public FBFilterArgs(string i_NameOrTitle, string i_MessageOrDescription, DateTime? i_StartDateOrPostDate, bool i_IsCaseSensitive)
        {
            NameOrTitle = i_NameOrTitle;
            MessageOrDescription = i_MessageOrDescription;
            StartDateOrPostDate = i_StartDateOrPostDate;
            IsCaseSensitive = i_IsCaseSensitive;
        }
    }
}

﻿namespace App
{
    internal partial class FormDocumentComposer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label sizeLabel;
            System.Windows.Forms.Label fontNameLabel;
            System.Windows.Forms.Label fieldNameLabel;
            System.Windows.Forms.Label documentDescriptionsRawMaterialLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDocumentComposer));
            this.dataGridViewPrintFields = new System.Windows.Forms.DataGridView();
            this.groupBoxFieldProperties = new System.Windows.Forms.GroupBox();
            this.panelTextColor = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.fontNameComboBox = new System.Windows.Forms.ComboBox();
            this.sizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.isCheckedToPrintCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.labelLinePreview = new System.Windows.Forms.Label();
            this.groupBoxPreview = new System.Windows.Forms.GroupBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.fieldNameLabel1 = new System.Windows.Forms.Label();
            this.textBoxTitleDescription = new System.Windows.Forms.TextBox();
            this.checkBoxIncludeGraph = new System.Windows.Forms.CheckBox();
            this.documentUserInfoRawMaterialsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printDocumentAbstractComposerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            sizeLabel = new System.Windows.Forms.Label();
            fontNameLabel = new System.Windows.Forms.Label();
            fieldNameLabel = new System.Windows.Forms.Label();
            documentDescriptionsRawMaterialLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintFields)).BeginInit();
            this.groupBoxFieldProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeNumericUpDown)).BeginInit();
            this.groupBoxPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentUserInfoRawMaterialsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDocumentAbstractComposerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // sizeLabel
            // 
            sizeLabel.AutoSize = true;
            sizeLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sizeLabel.Location = new System.Drawing.Point(7, 94);
            sizeLabel.Name = "sizeLabel";
            sizeLabel.Size = new System.Drawing.Size(83, 21);
            sizeLabel.TabIndex = 0;
            sizeLabel.Text = "Font size:";
            // 
            // fontNameLabel
            // 
            fontNameLabel.AutoSize = true;
            fontNameLabel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            fontNameLabel.Location = new System.Drawing.Point(7, 39);
            fontNameLabel.Name = "fontNameLabel";
            fontNameLabel.Size = new System.Drawing.Size(97, 21);
            fontNameLabel.TabIndex = 2;
            fontNameLabel.Text = "Font Name:";
            // 
            // fieldNameLabel
            // 
            fieldNameLabel.AutoSize = true;
            fieldNameLabel.Location = new System.Drawing.Point(631, 111);
            fieldNameLabel.Name = "fieldNameLabel";
            fieldNameLabel.Size = new System.Drawing.Size(83, 17);
            fieldNameLabel.TabIndex = 46;
            fieldNameLabel.Text = "Field Name:";
            // 
            // documentDescriptionsRawMaterialLabel
            // 
            documentDescriptionsRawMaterialLabel.AutoSize = true;
            documentDescriptionsRawMaterialLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            documentDescriptionsRawMaterialLabel.Location = new System.Drawing.Point(12, 39);
            documentDescriptionsRawMaterialLabel.Name = "documentDescriptionsRawMaterialLabel";
            documentDescriptionsRawMaterialLabel.Size = new System.Drawing.Size(224, 20);
            documentDescriptionsRawMaterialLabel.TabIndex = 47;
            documentDescriptionsRawMaterialLabel.Text = "Document Description / Title";
            // 
            // dataGridViewPrintFields
            // 
            this.dataGridViewPrintFields.AllowUserToAddRows = false;
            this.dataGridViewPrintFields.AllowUserToDeleteRows = false;
            this.dataGridViewPrintFields.AllowUserToResizeRows = false;
            this.dataGridViewPrintFields.AutoGenerateColumns = false;
            this.dataGridViewPrintFields.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPrintFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPrintFields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fieldNameDataGridViewTextBoxColumn,
            this.textValueDataGridViewTextBoxColumn});
            this.dataGridViewPrintFields.DataSource = this.documentUserInfoRawMaterialsListBindingSource;
            this.dataGridViewPrintFields.Location = new System.Drawing.Point(12, 111);
            this.dataGridViewPrintFields.Margin = new System.Windows.Forms.Padding(3, 2, 0, 2);
            this.dataGridViewPrintFields.Name = "dataGridViewPrintFields";
            this.dataGridViewPrintFields.RowHeadersVisible = false;
            this.dataGridViewPrintFields.RowTemplate.Height = 25;
            this.dataGridViewPrintFields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPrintFields.Size = new System.Drawing.Size(587, 244);
            this.dataGridViewPrintFields.TabIndex = 0;
            this.dataGridViewPrintFields.SelectionChanged += new System.EventHandler(this.dataGridViewPrintFields_SelectionChanged);
            // 
            // groupBoxFieldProperties
            // 
            this.groupBoxFieldProperties.Controls.Add(this.panelTextColor);
            this.groupBoxFieldProperties.Controls.Add(this.label2);
            this.groupBoxFieldProperties.Controls.Add(fontNameLabel);
            this.groupBoxFieldProperties.Controls.Add(this.fontNameComboBox);
            this.groupBoxFieldProperties.Controls.Add(sizeLabel);
            this.groupBoxFieldProperties.Controls.Add(this.sizeNumericUpDown);
            this.groupBoxFieldProperties.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFieldProperties.Location = new System.Drawing.Point(628, 135);
            this.groupBoxFieldProperties.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxFieldProperties.Name = "groupBoxFieldProperties";
            this.groupBoxFieldProperties.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxFieldProperties.Size = new System.Drawing.Size(368, 181);
            this.groupBoxFieldProperties.TabIndex = 39;
            this.groupBoxFieldProperties.TabStop = false;
            // 
            // panelTextColor
            // 
            this.panelTextColor.Location = new System.Drawing.Point(141, 137);
            this.panelTextColor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelTextColor.Name = "panelTextColor";
            this.panelTextColor.Size = new System.Drawing.Size(101, 32);
            this.panelTextColor.TabIndex = 6;
            this.panelTextColor.Click += new System.EventHandler(this.panelTextColor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Color:";
            // 
            // fontNameComboBox
            // 
            this.fontNameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.documentUserInfoRawMaterialsListBindingSource, "FontName", true));
            this.fontNameComboBox.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fontNameComboBox.FormattingEnabled = true;
            this.fontNameComboBox.Location = new System.Drawing.Point(141, 31);
            this.fontNameComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fontNameComboBox.Name = "fontNameComboBox";
            this.fontNameComboBox.Size = new System.Drawing.Size(207, 29);
            this.fontNameComboBox.TabIndex = 3;
            this.fontNameComboBox.SelectedIndexChanged += new System.EventHandler(this.fontNameComboBox_SelectedIndexChanged);
            // 
            // sizeNumericUpDown
            // 
            this.sizeNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.documentUserInfoRawMaterialsListBindingSource, "TextSize", true));
            this.sizeNumericUpDown.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sizeNumericUpDown.Location = new System.Drawing.Point(141, 86);
            this.sizeNumericUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sizeNumericUpDown.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.sizeNumericUpDown.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.sizeNumericUpDown.Name = "sizeNumericUpDown";
            this.sizeNumericUpDown.Size = new System.Drawing.Size(101, 28);
            this.sizeNumericUpDown.TabIndex = 1;
            this.sizeNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.sizeNumericUpDown.ValueChanged += new System.EventHandler(this.sizeNumericUpDown_ValueChanged);
            // 
            // isCheckedToPrintCheckBox
            // 
            this.isCheckedToPrintCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.documentUserInfoRawMaterialsListBindingSource, "isCheckedToPrint", true));
            this.isCheckedToPrintCheckBox.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.isCheckedToPrintCheckBox.Location = new System.Drawing.Point(639, 330);
            this.isCheckedToPrintCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.isCheckedToPrintCheckBox.Name = "isCheckedToPrintCheckBox";
            this.isCheckedToPrintCheckBox.Size = new System.Drawing.Size(243, 25);
            this.isCheckedToPrintCheckBox.TabIndex = 7;
            this.isCheckedToPrintCheckBox.Text = "Include selected field:";
            this.isCheckedToPrintCheckBox.UseVisualStyleBackColor = true;
            this.isCheckedToPrintCheckBox.CheckedChanged += new System.EventHandler(this.isCheckedToPrintCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(603, 136);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 210);
            this.label1.TabIndex = 43;
            this.label1.Text = "C\r\nH\r\nA\r\nN\r\nG\r\nE\r\n\r\n\r\nO\r\nR\r\nD\r\nE\r\nR";
            // 
            // buttonDown
            // 
            this.buttonDown.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonDown.Location = new System.Drawing.Point(599, 331);
            this.buttonDown.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(29, 25);
            this.buttonDown.TabIndex = 42;
            this.buttonDown.Text = "▼";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonUp.Location = new System.Drawing.Point(599, 111);
            this.buttonUp.Margin = new System.Windows.Forms.Padding(0, 4, 4, 0);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(29, 25);
            this.buttonUp.TabIndex = 41;
            this.buttonUp.Text = "▲";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // labelLinePreview
            // 
            this.labelLinePreview.AutoSize = true;
            this.labelLinePreview.Location = new System.Drawing.Point(5, 25);
            this.labelLinePreview.Name = "labelLinePreview";
            this.labelLinePreview.Size = new System.Drawing.Size(46, 17);
            this.labelLinePreview.TabIndex = 44;
            this.labelLinePreview.Text = "label3";
            // 
            // groupBoxPreview
            // 
            this.groupBoxPreview.Controls.Add(this.labelLinePreview);
            this.groupBoxPreview.Location = new System.Drawing.Point(12, 389);
            this.groupBoxPreview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxPreview.Name = "groupBoxPreview";
            this.groupBoxPreview.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxPreview.Size = new System.Drawing.Size(813, 81);
            this.groupBoxPreview.TabIndex = 45;
            this.groupBoxPreview.TabStop = false;
            this.groupBoxPreview.Text = "Preview";
            // 
            // buttonPrint
            // 
            this.buttonPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.Location = new System.Drawing.Point(831, 415);
            this.buttonPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(165, 55);
            this.buttonPrint.TabIndex = 46;
            this.buttonPrint.Text = "Send To Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // fieldNameLabel1
            // 
            this.fieldNameLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.documentUserInfoRawMaterialsListBindingSource, "FieldName", true));
            this.fieldNameLabel1.Location = new System.Drawing.Point(720, 111);
            this.fieldNameLabel1.Name = "fieldNameLabel1";
            this.fieldNameLabel1.Size = new System.Drawing.Size(100, 23);
            this.fieldNameLabel1.TabIndex = 47;
            this.fieldNameLabel1.Text = "label3";
            // 
            // textBoxTitleDescription
            // 
            this.textBoxTitleDescription.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.printDocumentAbstractComposerBindingSource, "DocumentDescriptionsRawMaterial", true));
            this.textBoxTitleDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTitleDescription.Location = new System.Drawing.Point(269, 39);
            this.textBoxTitleDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxTitleDescription.Name = "textBoxTitleDescription";
            this.textBoxTitleDescription.Size = new System.Drawing.Size(699, 26);
            this.textBoxTitleDescription.TabIndex = 48;
            // 
            // checkBoxIncludeGraph
            // 
            this.checkBoxIncludeGraph.AutoSize = true;
            this.checkBoxIncludeGraph.Location = new System.Drawing.Point(639, 360);
            this.checkBoxIncludeGraph.Name = "checkBoxIncludeGraph";
            this.checkBoxIncludeGraph.Size = new System.Drawing.Size(119, 21);
            this.checkBoxIncludeGraph.TabIndex = 49;
            this.checkBoxIncludeGraph.Text = "Include Graph";
            this.checkBoxIncludeGraph.UseVisualStyleBackColor = true;
            this.checkBoxIncludeGraph.CheckedChanged += new System.EventHandler(this.checkBoxIncludeGraph_CheckedChanged);
            // 
            // documentUserInfoRawMaterialsListBindingSource
            // 
            this.documentUserInfoRawMaterialsListBindingSource.DataSource = typeof(App.PrintField);
            // 
            // printDocumentAbstractComposerBindingSource
            // 
            this.printDocumentAbstractComposerBindingSource.DataSource = typeof(App.Builder.PrintDocumentAbstractComposer);
            // 
            // fieldNameDataGridViewTextBoxColumn
            // 
            this.fieldNameDataGridViewTextBoxColumn.DataPropertyName = "FieldName";
            this.fieldNameDataGridViewTextBoxColumn.HeaderText = "Field Name";
            this.fieldNameDataGridViewTextBoxColumn.Name = "fieldNameDataGridViewTextBoxColumn";
            this.fieldNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // textValueDataGridViewTextBoxColumn
            // 
            this.textValueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.textValueDataGridViewTextBoxColumn.DataPropertyName = "TextValue";
            this.textValueDataGridViewTextBoxColumn.HeaderText = "Text Value (editable)";
            this.textValueDataGridViewTextBoxColumn.Name = "textValueDataGridViewTextBoxColumn";
            // 
            // FormDocumentComposer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 473);
            this.Controls.Add(this.isCheckedToPrintCheckBox);
            this.Controls.Add(this.checkBoxIncludeGraph);
            this.Controls.Add(documentDescriptionsRawMaterialLabel);
            this.Controls.Add(this.textBoxTitleDescription);
            this.Controls.Add(fieldNameLabel);
            this.Controls.Add(this.fieldNameLabel1);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.groupBoxPreview);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.Controls.Add(this.groupBoxFieldProperties);
            this.Controls.Add(this.dataGridViewPrintFields);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "FormDocumentComposer";
            this.Text = "Document Designer";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrintFields)).EndInit();
            this.groupBoxFieldProperties.ResumeLayout(false);
            this.groupBoxFieldProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeNumericUpDown)).EndInit();
            this.groupBoxPreview.ResumeLayout(false);
            this.groupBoxPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentUserInfoRawMaterialsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printDocumentAbstractComposerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewPrintFields;
        private System.Windows.Forms.GroupBox groupBoxFieldProperties;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.NumericUpDown sizeNumericUpDown;
        private System.Windows.Forms.ComboBox fontNameComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelTextColor;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label labelLinePreview;
        private System.Windows.Forms.GroupBox groupBoxPreview;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn textValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource documentUserInfoRawMaterialsListBindingSource;
        private System.Windows.Forms.CheckBox isCheckedToPrintCheckBox;
        private System.Windows.Forms.Label fieldNameLabel1;
        private System.Windows.Forms.BindingSource printDocumentAbstractComposerBindingSource;
        private System.Windows.Forms.TextBox textBoxTitleDescription;
        private System.Windows.Forms.CheckBox checkBoxIncludeGraph;
    }
}
﻿using System;

namespace App
{
    public class CaseSensitiveFilterStrategy : IFilterStrategy
    {
        public bool CheckIfApplicable(string i_CompareTo, string i_TheCompared)
        {
            return string.IsNullOrEmpty(i_TheCompared) || (!string.IsNullOrEmpty(i_CompareTo) && i_CompareTo.IndexOf(i_TheCompared, StringComparison.Ordinal) > -1);
        }
    }
}

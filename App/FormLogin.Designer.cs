﻿namespace App
{
    internal partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.labelRememberedUsers = new System.Windows.Forms.Label();
            this.listBoxRememberedUsers = new System.Windows.Forms.ListBox();
            this.buttonDeletFromUsersList = new System.Windows.Forms.Button();
            this.checkBoxRememberUser = new System.Windows.Forms.CheckBox();
            this.buttonRememberedUsers = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.checkBoxSaveLastLoginDate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelRememberedUsers
            // 
            this.labelRememberedUsers.AutoSize = true;
            this.labelRememberedUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(84)))), ((int)(((byte)(146)))));
            this.labelRememberedUsers.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRememberedUsers.Location = new System.Drawing.Point(256, 8);
            this.labelRememberedUsers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRememberedUsers.Name = "labelRememberedUsers";
            this.labelRememberedUsers.Size = new System.Drawing.Size(169, 21);
            this.labelRememberedUsers.TabIndex = 15;
            this.labelRememberedUsers.Text = "Remembered Users";
            // 
            // listBoxRememberedUsers
            // 
            this.listBoxRememberedUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxRememberedUsers.FormattingEnabled = true;
            this.listBoxRememberedUsers.HorizontalScrollbar = true;
            this.listBoxRememberedUsers.ItemHeight = 22;
            this.listBoxRememberedUsers.Location = new System.Drawing.Point(237, 33);
            this.listBoxRememberedUsers.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxRememberedUsers.Name = "listBoxRememberedUsers";
            this.listBoxRememberedUsers.Size = new System.Drawing.Size(383, 136);
            this.listBoxRememberedUsers.TabIndex = 14;
            this.listBoxRememberedUsers.DoubleClick += new System.EventHandler(this.listBoxRememberedUsers_MouseDoubleClick);
            this.listBoxRememberedUsers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxRememberedUsers_KeyDown);
            // 
            // buttonDeletFromUsersList
            // 
            this.buttonDeletFromUsersList.BackColor = System.Drawing.Color.White;
            this.buttonDeletFromUsersList.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDeletFromUsersList.Font = new System.Drawing.Font("Tahoma", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeletFromUsersList.Location = new System.Drawing.Point(237, 183);
            this.buttonDeletFromUsersList.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDeletFromUsersList.Name = "buttonDeletFromUsersList";
            this.buttonDeletFromUsersList.Size = new System.Drawing.Size(383, 57);
            this.buttonDeletFromUsersList.TabIndex = 13;
            this.buttonDeletFromUsersList.Text = "Delete From List";
            this.buttonDeletFromUsersList.UseVisualStyleBackColor = false;
            this.buttonDeletFromUsersList.Click += new System.EventHandler(this.buttonDeletFromUsersList_Click);
            // 
            // checkBoxRememberUser
            // 
            this.checkBoxRememberUser.AutoSize = true;
            this.checkBoxRememberUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(84)))), ((int)(((byte)(146)))));
            this.checkBoxRememberUser.Font = new System.Drawing.Font("Arial", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRememberUser.Location = new System.Drawing.Point(10, 79);
            this.checkBoxRememberUser.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRememberUser.Name = "checkBoxRememberUser";
            this.checkBoxRememberUser.Size = new System.Drawing.Size(149, 25);
            this.checkBoxRememberUser.TabIndex = 12;
            this.checkBoxRememberUser.Text = "Remember Me";
            this.checkBoxRememberUser.UseVisualStyleBackColor = false;
            this.checkBoxRememberUser.CheckedChanged += new System.EventHandler(this.checkBoxRememberUser_CheckedChanged);
            // 
            // buttonRememberedUsers
            // 
            this.buttonRememberedUsers.BackColor = System.Drawing.Color.White;
            this.buttonRememberedUsers.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRememberedUsers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRememberedUsers.Location = new System.Drawing.Point(10, 14);
            this.buttonRememberedUsers.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRememberedUsers.Name = "buttonRememberedUsers";
            this.buttonRememberedUsers.Size = new System.Drawing.Size(210, 57);
            this.buttonRememberedUsers.TabIndex = 11;
            this.buttonRememberedUsers.Text = "Remembered Users >>";
            this.buttonRememberedUsers.UseVisualStyleBackColor = false;
            this.buttonRememberedUsers.Click += new System.EventHandler(this.buttonRememberedUsers_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.White;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogin.Font = new System.Drawing.Font("Tahoma", 25.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogin.Location = new System.Drawing.Point(10, 147);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(210, 94);
            this.buttonLogin.TabIndex = 10;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // checkBoxSaveLastLoginDate
            // 
            this.checkBoxSaveLastLoginDate.AutoSize = true;
            this.checkBoxSaveLastLoginDate.Checked = true;
            this.checkBoxSaveLastLoginDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSaveLastLoginDate.Enabled = false;
            this.checkBoxSaveLastLoginDate.Location = new System.Drawing.Point(10, 112);
            this.checkBoxSaveLastLoginDate.Name = "checkBoxSaveLastLoginDate";
            this.checkBoxSaveLastLoginDate.Size = new System.Drawing.Size(135, 21);
            this.checkBoxSaveLastLoginDate.TabIndex = 16;
            this.checkBoxSaveLastLoginDate.Text = "Sava Login Date";
            this.checkBoxSaveLastLoginDate.UseVisualStyleBackColor = true;
            this.checkBoxSaveLastLoginDate.CheckedChanged += new System.EventHandler(this.checkBoxSaveLastLoginDate_CheckedChanged);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(84)))), ((int)(((byte)(146)))));
            this.ClientSize = new System.Drawing.Size(229, 253);
            this.Controls.Add(this.checkBoxSaveLastLoginDate);
            this.Controls.Add(this.labelRememberedUsers);
            this.Controls.Add(this.listBoxRememberedUsers);
            this.Controls.Add(this.buttonDeletFromUsersList);
            this.Controls.Add(this.checkBoxRememberUser);
            this.Controls.Add(this.buttonRememberedUsers);
            this.Controls.Add(this.buttonLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRememberedUsers;
        private System.Windows.Forms.ListBox listBoxRememberedUsers;
        private System.Windows.Forms.Button buttonDeletFromUsersList;
        private System.Windows.Forms.CheckBox checkBoxRememberUser;
        private System.Windows.Forms.Button buttonRememberedUsers;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.CheckBox checkBoxSaveLastLoginDate;
    }
}
﻿namespace App
{
    using FacebookWrapper;

    public abstract class UserStorageManagerBase
    {
        public void StoreUser(LoginResult i_Result, AccountsStorage i_Storrage )
        {
            string uniqueName = CreateUniqueName(i_Result);
            SaveToStorage(i_Storrage, i_Result.LoggedInUser.Id, uniqueName, i_Result.AccessToken);
        }

        protected abstract string CreateUniqueName(LoginResult i_User);

        protected virtual void SaveToStorage(AccountsStorage i_Storrage, string i_UserID, string i_Name, string i_Identifier)
        {
            i_Storrage.AddToStorage(i_UserID, i_Name, i_Identifier);
        }
    }
}

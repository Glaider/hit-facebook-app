﻿using System.Drawing;

namespace App
{
    public class PrintField
    {
        public PrintField(string i_FieldName, string i_TextValue, bool i_isTextField = true, bool i_isCheckedToPrint = true)
        {
            FieldName = i_FieldName;
            TextValue = i_TextValue;
            TextColor = Color.Black;
            isTextField = i_isTextField;
            isCheckedToPrint = i_isCheckedToPrint;
            FontName = "Calibri";
            TextSize = 8;
        }

        public string FieldName { get; set; }

        public string TextValue { get; set; }

        public Color TextColor { get; set; }

        public bool isCheckedToPrint { get; set; }

        public bool isTextField { get; set; }

        public string FontName { get; set; }

        public int TextSize { get; set; }
    }
}

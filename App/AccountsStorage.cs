﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace App
{
    public class AccountsStorage
    {
        private readonly string r_FileName;

        public List<UserConfiguration> StorageList { get; private set; }

        public event Action<UserConfiguration> UserConfigurationsAddedToStorage;

        public event Action<UserConfiguration> UserConfigurationsRemovedFromStorage;

        public AccountsStorage(string i_FileName)
        {
            r_FileName = i_FileName;
            StorageList = loadFromFile();
        }

        public void AddToStorage(string i_UserID, string i_UniqueName, string i_AccessToken)
        {
            foreach (UserConfiguration config in StorageList)
            {
                if (config.UserID == i_UserID)
                {
                    config.UniqueName = i_UniqueName;
                    config.AccessToken = i_AccessToken;
                    return;
                }
            }

            UserConfiguration userConfigurationToStore = new UserConfiguration(i_UserID, i_UniqueName, i_AccessToken);
            StorageList.Add(userConfigurationToStore);
            if (UserConfigurationsAddedToStorage != null)
            {
                UserConfigurationsAddedToStorage.Invoke(userConfigurationToStore);
            }
        }

        public void RemoveFromStorage(UserConfiguration i_UserConfigToRemove)
        {
            StorageList.Remove(i_UserConfigToRemove);
            if (UserConfigurationsRemovedFromStorage != null)
            {
                UserConfigurationsRemovedFromStorage.Invoke(i_UserConfigToRemove);
            }
        }

        public void SaveToFile()
        {
            using (FileStream stream = new FileStream(r_FileName, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<UserConfiguration>));
                serializer.Serialize(stream, StorageList);
            }
        }

        public bool IsStorageEmpty
        {
            get
            {
                return StorageList.Count == 0;
            }
        }

        private List<UserConfiguration> loadFromFile()
        {
            List<UserConfiguration> loadedFromFileList = new List<UserConfiguration>();
            if (File.Exists(r_FileName))
            {
                using (FileStream stream = new FileStream(r_FileName, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<UserConfiguration>));
                    loadedFromFileList = (List<UserConfiguration>)serializer.Deserialize(stream);
                }
            }

            return loadedFromFileList;
        }

        public class UserConfiguration
        {
            public string AccessToken { get; set; }

            public string UniqueName { get; set; }

            public string UserID { get; set; }

            public UserConfiguration(string i_UserID, string i_UserName, string i_AccessToken)
            {
                UniqueName = i_UserName;
                AccessToken = i_AccessToken;
                UserID = i_UserID;
            }

            public UserConfiguration()
            {
            }

            public override string ToString()
            {
                return UniqueName;
            }
        }
    }
}

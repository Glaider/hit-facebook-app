﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using App.Builder;

namespace App
{
    internal partial class FormDocumentComposer : Form
    {
        private IPrintComposerAdaptable Adaptee { get; set; }

        private PrintDocumentAbstractBuilder Builder { get; set; }

        private PrintDocumentAbstractComposer Composer { get; set; }

        private Image ChartImage { get; set; }

        public FormDocumentComposer(IPrintComposerAdaptable i_Ataptee, Image i_ChartImage)
        {
            InitializeComponent();
            ChartImage = i_ChartImage;
            Adaptee = i_Ataptee;
            checkBoxIncludeGraph.Enabled = ChartImage != null;
            Builder = new DescriptionBodyPicturesPrintDocumentBuilder();
            Composer = new DescriptionBodyPicturesDocumentComposer(Builder);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            setComposerRawMaterials();
            fillFontsToComboBox();
        }

        private void fillFontsToComboBox()
        {          
            foreach (FontFamily font in FontFamily.Families)
            {
                fontNameComboBox.Items.Add(font.Name);
            }
        }

        private void setComposerRawMaterials()
        {
            Composer.AddDocumentUserInfoRawMaterial(getAdaptedPrintList());
            documentUserInfoRawMaterialsListBindingSource.DataSource = Composer.DocumentUserInfoRawMaterialsList;
            printDocumentAbstractComposerBindingSource.DataSource = Composer;
        }

        private List<PrintField> getAdaptedPrintList()
        {
            List<PrintField> adaptedPrintFields = new List<PrintField>
                                                      {
                                                          Adaptee.GetNamePrintField,
                                                          Adaptee.GetBirthDayPrintField,
                                                          Adaptee.GetEmailPrintField,
                                                          Adaptee.GetLinkPrintField,
                                                          Adaptee.GetLocalePrintField,
                                                          Adaptee.GetBioPrintField,
                                                          Adaptee.GetProfilePicturePrintField
                                                      };
            return adaptedPrintFields;
        }

        private void panelTextColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                panelTextColor.BackColor = colorDialog1.Color;

                if (dataGridViewPrintFields.CurrentRow != null)
                {
                    PrintField printField = dataGridViewPrintFields.CurrentRow.DataBoundItem as PrintField;
                    if (printField != null)
                    {
                        printField.TextColor = colorDialog1.Color;
                        labelLinePreview.ForeColor = colorDialog1.Color;
                        labelLinePreview.Invalidate();
                    }
                }
            }
        }

        private void dataGridViewPrintFields_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewPrintFields.CurrentRow != null)
            {
                PrintField printField = dataGridViewPrintFields.CurrentRow.DataBoundItem as PrintField;
                if (printField != null)
                {
                    groupBoxFieldProperties.Enabled = printField.isTextField && printField.isCheckedToPrint;
                    panelTextColor.BackColor = printField.TextColor;
                    labelLinePreview.ForeColor = printField.TextColor;
                    labelLinePreview.Text = string.Format("{0}: {1} ", printField.FieldName, printField.TextValue);
                }
            }
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            const bool v_DataChanged = true;

            Composer.MoveUserInfoFieldUp(documentUserInfoRawMaterialsListBindingSource.Current as PrintField);
            documentUserInfoRawMaterialsListBindingSource.ResetBindings(!v_DataChanged);
            documentUserInfoRawMaterialsListBindingSource.Position -= 1;
        }
     
        private void buttonDown_Click(object sender, EventArgs e)
        {
            const bool v_DataChanged = true;
            Composer.MoveUserInfoFieldDown(documentUserInfoRawMaterialsListBindingSource.Current as PrintField);
            documentUserInfoRawMaterialsListBindingSource.ResetBindings(!v_DataChanged);
            documentUserInfoRawMaterialsListBindingSource.Position += 1;
        }

        private void sizeNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            labelLinePreview.Font = new Font(labelLinePreview.Font.FontFamily, (float)sizeNumericUpDown.Value);
            labelLinePreview.Invalidate();
        }

        private void fontNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelLinePreview.Font = new Font(fontNameComboBox.SelectedItem as string, labelLinePreview.Font.Size);
            labelLinePreview.Invalidate();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                Composer.Construct();
                PrintDocument printDoc = Builder.ResultPrintDocument;
                printDoc.PrinterSettings = printDialog.PrinterSettings;
                printDoc.Print();
            }
        }

        private void checkBoxIncludeGraph_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox check = sender as CheckBox;
            if (check != null)
            {
                Composer.AddDocumentUserChartImageRawMaterial(check.Checked ? ChartImage : null);
            }
        }

        private void isCheckedToPrintCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chechBox = sender as CheckBox;
            if (chechBox != null)
            {
                groupBoxFieldProperties.Enabled = chechBox.Checked;
            }
        }      
    }
}

﻿using System;
using System.Windows.Forms;
using Facebook;
using FacebookWrapper;

namespace App
{
    internal partial class FormLogin : Form
    {
        private bool IsRememberedUsersOpen { get; set; }

        private FacebookAppSession FacebookAppSession { get; set; }

        private AccountsStorage Storage { get; set; }

        private UserStorageManagerBase StorageMethod { get; set; }

        public FormLogin()
        {
            InitializeComponent();
            Storage = new AccountsStorage("AccountsSettings.xml");
            FacebookAppSession = new FacebookAppSession();
            Storage.UserConfigurationsAddedToStorage += userConfiguration_AddedToStorage;
            Storage.UserConfigurationsRemovedFromStorage += userConfiguration_RemovedFromStorage;
            StorageMethod = new UserStorageByNameMailLastLoginDate();
            FacebookService.s_CollectionLimit = 1000;
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (IsRememberedUsersOpen && listBoxRememberedUsers.SelectedIndex > -1)
            {
                logInRememberedUser();
            }
            else
            {
                logInNewUser();
            }
        }

        private void startAccountBoard()
        {
            if (FacebookAppSession.GetLoginData != null)
            {
                Hide();
                DialogResult facebookAccountResult = new FormFacebookAccountBoard(new FacebookAppSessionProxy(FacebookAppSession)).ShowDialog();
                finishAccountBoard(facebookAccountResult);
            }
            else
            {
                if (FacebookAppSession.GetLoginData != null)
                {
                    MessageBox.Show(FacebookAppSession.GetLoginData.ErrorMessage);
                }
            }
        }

        private void userConfiguration_AddedToStorage(AccountsStorage.UserConfiguration i_UserConfiguration)
        {
            listBoxRememberedUsers.Items.Add(i_UserConfiguration);
            if (Storage.StorageList.Count == 1)
            {
                buttonRememberedUsers.Enabled = true;
            }
        }

        private void userConfiguration_RemovedFromStorage(AccountsStorage.UserConfiguration i_UserConfiguration)
        {
            listBoxRememberedUsers.Items.Remove(i_UserConfiguration);
        }

        private void storeLoggedInUserConfiguration()
        {
            StorageMethod.StoreUser(FacebookAppSession.GetLoginData, Storage);

            if (listBoxRememberedUsers.Items.Count == 1)
            {
                buttonRememberedUsers.Enabled = true;
            }
        }

        private void logInRememberedUser()
        {
            string knownAccsesToken = ((AccountsStorage.UserConfiguration)listBoxRememberedUsers.SelectedItem).AccessToken;
            if (knownAccsesToken != null)
            {
                try
                {
                    FacebookAppSession.InitializeData(knownAccsesToken);
                }
                catch (FacebookApiException ex)
                {
                    MessageBox.Show(this, ex.Message, @"Ooops...", MessageBoxButtons.OK);
                    Storage.RemoveFromStorage((AccountsStorage.UserConfiguration)listBoxRememberedUsers.SelectedItem);
                    FacebookAppSession.Finish();
                }
                catch (WebExceptionWrapper ex)
                {
                    MessageBox.Show(this, string.Format("{0}{1}{2}", ex.Message, Environment.NewLine, "(Tip: Check Connection)"), @"Oooops...", MessageBoxButtons.OK);
                }
                catch
                {
                    MessageBox.Show(this, @"Problem Occured...", @"Oooops...", MessageBoxButtons.OK);
                }

                storeLoggedInUserConfiguration();
                if (FacebookAppSession.GetLoginData != null)
                {
                    startAccountBoard();
                }
            }
        }

        private void logInNewUser()
        {
            try
            {
                FacebookAppSession.InitializeData();
                if (!string.IsNullOrEmpty(FacebookAppSession.GetLoginData.AccessToken))
                {
                    if (checkBoxRememberUser.Checked)
                    {
                        storeLoggedInUserConfiguration();
                    }

                    startAccountBoard();
                }
                else
                {
                    MessageBox.Show(FacebookAppSession.GetLoginData.ErrorMessage);
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Problem occured..." + ex.Message);
            }
        }

        private void finishAccountBoard(DialogResult i_FacebookAccountBoardResult)
        {
            switch (i_FacebookAccountBoardResult)
            {
                case DialogResult.Retry:
                    fillSavedUsersListBox();
                    Show();
                    if (IsRememberedUsersOpen)
                    {
                        buttonRememberedUsers.PerformClick();
                    }

                    break;
                case DialogResult.Cancel:
                    Close();
                    break;
            }
        }

        private void buttonRememberedUsers_Click(object sender, EventArgs e)
        {
            const int k_RememberUsersFormWidthChange = 300;

            if (!IsRememberedUsersOpen)
            {
                if (listBoxRememberedUsers.Items.Count > 0)
                {
                    listBoxRememberedUsers.SelectedIndex = 0;
                }

                Width += k_RememberUsersFormWidthChange;
                buttonRememberedUsers.Text = @"<< Close Remembered Users";
                checkBoxRememberUser.Checked = false;
            }
            else
            {
                Width -= k_RememberUsersFormWidthChange;
                buttonRememberedUsers.Text = @"Remembered Users >>";
                checkBoxSaveLastLoginDate.Checked = true;
            }

            IsRememberedUsersOpen = !IsRememberedUsersOpen;
            checkBoxSaveLastLoginDate.Enabled = !checkBoxSaveLastLoginDate.Enabled;
            checkBoxRememberUser.Enabled = !checkBoxRememberUser.Enabled;
        }

        protected override void OnClosed(EventArgs e)
        {
            Storage.SaveToFile();
            base.OnClosed(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            fillSavedUsersListBox();
        }

        private void fillSavedUsersListBox()
        {
            try
            {
                if (Storage.IsStorageEmpty)
                {
                    buttonRememberedUsers.Enabled = false;
                }
                else
                {
                    listBoxRememberedUsers.Items.Clear();
                    foreach (var userConfig in Storage.StorageList)
                    {
                        userConfiguration_AddedToStorage(userConfig);
                    }
                }
            }
            catch
            {
                listBoxRememberedUsers.Items.Add("Failed to load data");
                listBoxRememberedUsers.Enabled = false;
            }
        }

        private void buttonDeletFromUsersList_Click(object sender, EventArgs e)
        {
            Storage.RemoveFromStorage((AccountsStorage.UserConfiguration)listBoxRememberedUsers.SelectedItem);
            listBoxRememberedUsers.SelectedIndex = 0;
            if (Storage.IsStorageEmpty)
            {
                buttonRememberedUsers.PerformClick();
                buttonRememberedUsers.Enabled = false;
            }
        }

        private void listBoxRememberedUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                logInRememberedUser();
            }
        }

        private void listBoxRememberedUsers_MouseDoubleClick(object sender, EventArgs e)
        {
            logInRememberedUser();
        }

        private void checkBoxSaveLastLoginDate_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            if (checkBox != null)
            {
                if (checkBox.Checked)
                {
                    StorageMethod = new UserStorageByNameMailLastLoginDate();
                }
                else
                {
                    StorageMethod = new UserStorageByNameMail();
                }
            }
        }

        private void checkBoxRememberUser_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            if (checkBox != null)
            {
                checkBoxSaveLastLoginDate.Enabled = checkBox.Checked;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace A16_Ex02_Stephan_321178253_Alex_323260620
{
    using System.Windows.Forms.DataVisualization.Charting;

    using FacebookWrapper.ObjectModel;

    public class AfterDateStatisticsFinder : StatisticsFinderBase
    {
        public AfterDateStatisticsFinder(Chart i_ChartToDisplay, IList<Event> i_EventList, DateTime i_DateChoosedForStatistics)
            : base(i_ChartToDisplay, i_EventList, i_DateChoosedForStatistics)
        {
                
        }

        public override int FindStartDateIndex()
        {
            int index = -1;
            for (int i = EventList.Count - 1; i >= 0; i--)
            {
                var startTime = EventList[i].StartTime;
                if (startTime != null)
                {
                    int comparison = startTime.Value.Date.CompareTo(DateChoosedForStatistics);

                    if (comparison == -1)
                    {
                        continue;
                    }
                    index = i;
                    break;
                }
            }
            return index;
        }

        public override void Find7DayStats(int i_Index)
        {
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;

namespace App
{
    public class FacebookAppSession : IDataProducer
    {
        public LoginResult Result { get; private set; }

        public void Finish()
        {
            FacebookService.Logout(null);
            Result = null;
        }

        public void InitializeData(string i_Token = null)
        {
            Result = string.IsNullOrEmpty(i_Token) ? FacebookService.Login("909882489077378", "email", "user_photos", "publish_actions", "user_status", "user_friends", "user_posts", "user_events", "user_birthday", "user_about_me") : FacebookService.Connect(i_Token);
        }

        public void Post(string i_TextPost)
        {
            Result.LoggedInUser.PostStatus(i_TextPost);
        }

        public void FilterPosts(FBFilterArgs i_FilterArgs)
        {
            throw new NotImplementedException();
        }

        public IList<Post> GetPosts
        {
            get
            {
                return Result.LoggedInUser.Posts;
            }
        }

        public void FilterEvents(FBFilterArgs i_FilterArgs)
        {
            throw new NotImplementedException();
        }

        public IList<Event> GetEvents
        {
            get
            {
                return Result.LoggedInUser.Events;
            }
        }

        public LoginResult GetLoginData 
        {
            get
            {
                return Result;
            }
        }

        public void ReFetch()
        {
            Result.LoggedInUser.ReFetch();
        }
    }
}

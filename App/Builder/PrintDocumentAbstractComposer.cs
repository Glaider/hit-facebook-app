﻿using System.Collections.Generic;
using System.Drawing;

namespace App.Builder
{
    public abstract class PrintDocumentAbstractComposer
    {
        protected PrintDocumentAbstractBuilder Builder { get; set; }

        protected Image DocumentUserChartImageRawMaterial { get; set; }

        public string DocumentDescriptionsRawMaterial { get; set; }

        public List<PrintField> DocumentUserInfoRawMaterialsList { get; protected set; }

        public abstract void AddDocumentDescriptionRawMaterial(string i_DocumentDescription);

        public abstract void AddDocumentUserInfoRawMaterial(List<PrintField> i_PrintFields);

        public abstract void AddDocumentUserChartImageRawMaterial(Image i_ChartImage);

        public abstract void MoveUserInfoFieldUp(PrintField i_PrintField);

        public abstract void MoveUserInfoFieldDown(PrintField i_PrintField);

        public abstract void Construct();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Net;

namespace App.Builder
{
    public class DescriptionBodyPicturesPrintDocumentBuilder : PrintDocumentAbstractBuilder
    {
        private const int linePadding = 5;

        public Image ChartImage { get; private set; }

        public float WriteAreaHeight { get; private set; }
      
        public string Title { get; private set; }

        public List<PrintField> UserInfoList { get; private set; }
           
        public override void BuildDocumentPrintDate()
        {
            ResultPrintDocument.PrintPage += datePrintEventConstruct;
        }

        private void datePrintEventConstruct(object sender, PrintPageEventArgs ev)
        {
            int widthOfPage = ev.MarginBounds.Width + (ev.MarginBounds.X * 2);
            Font dateFont = new Font("Calibri", 14, FontStyle.Italic);
            string dateString = "Print date: " + DateTime.Today.ToShortDateString();
            float datePadding = (widthOfPage / 2) - (ev.Graphics.MeasureString(dateString, dateFont).Width / 2);
            ev.Graphics.DrawString(dateString, dateFont, Brushes.Black, datePadding, ev.MarginBounds.Bottom);
            WriteAreaHeight += ev.Graphics.MeasureString(Title, dateFont).Height + linePadding;
            ResultPrintDocument.PrintPage -= datePrintEventConstruct;
        }

        public override void BuildDocumentChartArea(Image i_ChartImage)
        {
            ChartImage = i_ChartImage;

            ResultPrintDocument.PrintPage += documentChartAreaEventConstruct;
        }

        private Image scaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        public override void BuilDocumentDescriptionArea(string i_DescriptionsTxt)
        {
            Title = i_DescriptionsTxt;
            ResultPrintDocument.PrintPage += descriptionPrintEventConstruct;
        }

        public override void BuildDocumentUserInfoArea(List<PrintField> i_UserInfoList)
        {
            UserInfoList = i_UserInfoList;
            ResultPrintDocument.PrintPage += userInfoPrintEventConstruct;
        }

        private void documentChartAreaEventConstruct(object sender, PrintPageEventArgs ev)
        {
            if (ChartImage != null)
            {
                Image img = scaleImage(ChartImage, 600, 600);
                ev.Graphics.DrawImage(img, new Point(ev.MarginBounds.Left, (int)WriteAreaHeight));
                WriteAreaHeight += ChartImage.Height + (linePadding * 3);
            }

            ResultPrintDocument.PrintPage -= documentChartAreaEventConstruct;
        }

        private void descriptionPrintEventConstruct(object sender, PrintPageEventArgs ev)
        {
            WriteAreaHeight = ev.MarginBounds.Top;
            Font descriptionFont = new Font("Calibri", 24, FontStyle.Underline);
            ev.Graphics.DrawString(Title, descriptionFont, Brushes.Black, ev.MarginBounds.Left, WriteAreaHeight);
            WriteAreaHeight += ev.Graphics.MeasureString(Title, descriptionFont).Height + linePadding;
            ResultPrintDocument.PrintPage -= descriptionPrintEventConstruct;
        }

        private void userInfoPrintEventConstruct(object sender, PrintPageEventArgs ev)
        {
            foreach (PrintField printField in UserInfoList)
            {
                if (printField.isCheckedToPrint)
                {
                    if (printField.isTextField)
                    {
                        Font lineFont = new Font(printField.FontName, printField.TextSize, FontStyle.Regular);
                        SolidBrush solidBrush = new SolidBrush(printField.TextColor);
                        ev.Graphics.DrawString(string.Format("{0}: {1}", printField.FieldName, printField.TextValue), lineFont, solidBrush, ev.MarginBounds.Left, WriteAreaHeight);
                        WriteAreaHeight += ev.Graphics.MeasureString(printField.FieldName, lineFont).Height + linePadding;
                    }
                    else
                    {
                        WebClient webClient = new WebClient();
                        Image image;
                        try
                        {
                            var downloadData = webClient.DownloadData(printField.TextValue);
                            Stream stream = new MemoryStream(downloadData);
                            image = Image.FromStream(stream);
                        }
                        catch 
                        {
                            image = Properties.Resources.No_Picture_Available;
                        }

                        ev.Graphics.DrawImage(image, new Point(ev.MarginBounds.Left, (int)WriteAreaHeight));
                        WriteAreaHeight += image.Height + (linePadding * 3);
                        ResultPrintDocument.PrintPage -= userInfoPrintEventConstruct;
                    }
                }
            }
        }
    }
}

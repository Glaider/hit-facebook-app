﻿using System.Collections.Generic;
using System.Drawing;

namespace App.Builder
{
    public class DescriptionBodyPicturesDocumentComposer : PrintDocumentAbstractComposer
    {
        public DescriptionBodyPicturesDocumentComposer(PrintDocumentAbstractBuilder i_Builder)
        {
            Builder = i_Builder;
            DocumentUserInfoRawMaterialsList = new List<PrintField>();
            DocumentDescriptionsRawMaterial = "Document";
        }

        public override void Construct()
        {
            Builder.BuilDocumentDescriptionArea(DocumentDescriptionsRawMaterial);
            Builder.BuildDocumentUserInfoArea(DocumentUserInfoRawMaterialsList);
            Builder.BuildDocumentChartArea(DocumentUserChartImageRawMaterial);
            Builder.BuildDocumentPrintDate();
        }
       
        public override void AddDocumentDescriptionRawMaterial(string i_DocumentDescription)
        {
            DocumentDescriptionsRawMaterial = i_DocumentDescription ?? string.Empty;
        }

        public override void AddDocumentUserChartImageRawMaterial(Image i_ChartImage)
        {
            DocumentUserChartImageRawMaterial = i_ChartImage;
        }

        public override void AddDocumentUserInfoRawMaterial(List<PrintField> i_PrintFieldList)
        {
            DocumentUserInfoRawMaterialsList = i_PrintFieldList;
        }

        public override void MoveUserInfoFieldUp(PrintField i_PrintField)
        {
            int index = DocumentUserInfoRawMaterialsList.IndexOf(i_PrintField);

            DocumentUserInfoRawMaterialsList.RemoveAt(index);
            index -= (index > 0) ? 1 : 0;
            DocumentUserInfoRawMaterialsList.Insert(index, i_PrintField);
        }

        public override void MoveUserInfoFieldDown(PrintField i_PrintField)
        {
            int index = DocumentUserInfoRawMaterialsList.IndexOf(i_PrintField);

            DocumentUserInfoRawMaterialsList.RemoveAt(index);
            index += (index < DocumentUserInfoRawMaterialsList.Count) ? 1 : 0;
            DocumentUserInfoRawMaterialsList.Insert(index, i_PrintField);
        }
    }
}

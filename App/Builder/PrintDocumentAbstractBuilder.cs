﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;

namespace App.Builder
{
    public abstract class PrintDocumentAbstractBuilder
    {
        public PrintDocument ResultPrintDocument { get; protected set; }

        public abstract void BuilDocumentDescriptionArea(string i_DescriptionsList);

        public abstract void BuildDocumentUserInfoArea(List<PrintField> i_UserInfoList);

        public abstract void BuildDocumentChartArea(Image i_ChartImage);

        public abstract void BuildDocumentPrintDate();
        
        protected PrintDocumentAbstractBuilder()
        {
            ResultPrintDocument = new PrintDocument();
        }
    }
}

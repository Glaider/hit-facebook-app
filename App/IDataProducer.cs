﻿using System.Collections.Generic;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;

namespace App
{
    public interface IDataProducer
    {    
        IList<Post> GetPosts { get; }

        IList<Event> GetEvents { get; }

        LoginResult GetLoginData { get; }

        void Post(string i_TextPost);

        void FilterEvents(FBFilterArgs i_FilterArgs);

        void FilterPosts(FBFilterArgs i_FilterArgs);

        void InitializeData(string i_Token = null);

        void ReFetch();

        void Finish();
    }
}

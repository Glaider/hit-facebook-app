﻿namespace App
{
    public class PrintComposerAdapter : IPrintComposerAdaptable
    {
        private IDataProducer Adaptee { get; set; }

        private const string notAvailable = @"<<Not Available>>";

        private const bool v_isTextField = true;

        public PrintComposerAdapter(IDataProducer i_Adaptee)
        {
            Adaptee = i_Adaptee;
        }

        public PrintField GetNamePrintField
        {
            get
            {
                return new PrintField("Name", Adaptee.GetLoginData.LoggedInUser.Name ?? notAvailable);
            }
        }

        public PrintField GetBirthDayPrintField
        {
            get
            {
                return new PrintField("Birth Day", Adaptee.GetLoginData.LoggedInUser.Birthday ?? notAvailable);
            }
        }

        public PrintField GetEmailPrintField
        {
            get
            {
                return new PrintField("Email", Adaptee.GetLoginData.LoggedInUser.Email ?? notAvailable);
            }
        }

        public PrintField GetLinkPrintField
        {
            get
            {
                return new PrintField("Link", Adaptee.GetLoginData.LoggedInUser.Link ?? notAvailable);
            }
        }

        public PrintField GetLocalePrintField
        {
            get
            {
                return new PrintField("Locale", Adaptee.GetLoginData.LoggedInUser.Locale ?? notAvailable);
            }
        }

        public PrintField GetBioPrintField
        {
            get
            {
                return new PrintField("Bio", Adaptee.GetLoginData.LoggedInUser.Bio ?? notAvailable);
            }
        }

        public PrintField GetProfilePicturePrintField
        {
            get
            {
                return new PrintField("Profile Picture", Adaptee.GetLoginData.LoggedInUser.PictureSqaureURL ?? notAvailable, !v_isTextField);
            }
        }
    }
}

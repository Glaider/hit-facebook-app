﻿namespace App
{
    public class UserStorageByNameMail : UserStorageManagerBase
    {
        protected override string CreateUniqueName(FacebookWrapper.LoginResult i_User)
        {
            string logedUserEMail = i_User.LoggedInUser.Email ?? "No Email";
            string uniqName = string.Format("{0} ({1})", i_User.LoggedInUser.Name, logedUserEMail);
            return uniqName;
        }
    }
}

﻿using System;

namespace App
{
    public class UserStorageByNameMailLastLoginDate : UserStorageManagerBase
    {
        protected override string CreateUniqueName(FacebookWrapper.LoginResult i_User)
        {
            string logedUserEMail = i_User.LoggedInUser.Email ?? "No Email";
            string uniqName = string.Format("{0} ({1}) Last seen: {2}", i_User.LoggedInUser.Name, logedUserEMail, DateTime.Now);
            return uniqName;
        }
    }
}
